<?php

class Gotobackend extends CI_Controller {
    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('user_model');
        $this->user_model->check_role();
    }

    public function login()
    {
        // event register button
        if ($this->input->post('btn-reg'))
        {
            $this->user_model->register();
            if ($this->user_model->error_count != 0) {
                $this->data['error']    = $this->user_model->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('gotobackend/login');
            }
        }

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new user created
            $this->session->unset_userdata('tmp_success');
            $this->data['tmp_success'] = 1;
        }



        // event login button
        if ($this->input->post('btn-login'))
        {
            $this->load->model('user_model');
            $this->user_model->check_login();
            if ($this->user_model->error_count != 0) {
                $this->data['error_login']    = $this->user_model->error;
            } else {
                redirect('backend/dashboard');
            }
        }



        $this->load->view('account/v_login', $this->data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('gotobackend/login');
    }

    public function index()
    {
        redirect('backend/dashboard', $this->data);
    }
}
