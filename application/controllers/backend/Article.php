<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('article_model');
    $this->load->library('encrypt');
    $this->load->model('admin_model');
    $this->load->model('user_model');
    $this->load->model('info_model');
    $this->load->library('upload');
    $this->load->library('pagination');
    $this->load->helper('slug_helper');
    $this->user_model->check_role();

    if (!$this->session->userdata('cibb_user_id')) {
      redirect('gotobackend/login');
    }

  }


  function time_ago($timestamp){

    //type cast, current time, difference in timestamps
    $timestamp      = (int) $timestamp;
    $current_time   = time();
    $diff           = $current_time - $timestamp;

    //intervals in seconds
    $intervals      = array (
      'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );

    //now we just find the difference
    if ($diff == 0)
    {
      return 'just now';
    }
    if ($diff < 60)
    {
      return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
    }
    if ($diff >= 60 && $diff < $intervals['hour'])
    {
      $diff = floor($diff/$intervals['minute']);
      return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
    }
    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
      $diff = floor($diff/$intervals['hour']);
      return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
    }
    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
      $diff = floor($diff/$intervals['day']);
      return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
    }
    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
      $diff = floor($diff/$intervals['week']);
      return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
    }
    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
      $diff = floor($diff/$intervals['month']);
      return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
    }
    if ($diff >= $intervals['year'])
    {
      $diff = floor($diff/$intervals['year']);
      return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
    }
  }

  // fungsi untuk mengambil data
  public function index()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');

    $search = array('pic_title' => $cari );

    $batas =  1000000; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->article_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $data['data'] = $this->article_model->get($batas,$offset,$search);

    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // user updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // user deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }
    $data['user']= $this->admin_model->get_by_id($this->session->userdata('cibb_user_id'));
    $this->data['title']   = 'Article';
    $this->load->view('backend/content/html', $this->data);
    $this->load->view('backend/content/article_list',$data);
  }

  // untuk menampilkan halaman tambah data
  public function add()
  {
    return $this->load->view('backend/upload_product');
  }

  public function insertdata()

  {
    date_default_timezone_set("ASIA/JAKARTA");
    $id   = $this->input->post('pic_id');
    $title   = $this->input->post('pic_title');
    $title   = $this->input->post('pic_title1');
    $category = $this->input->post('pic_category');
    $project = $this->input->post('pic_project');
    $project1 = $this->input->post('pic_project1');
    $desc = $this->input->post('pic_desc');
    $desc1 = $this->input->post('pic_desc1');
    $date_picker = $this->input->post('pic_date_picker');
    $slug = slug($this->input->post('pic_title',TRUE));


    $config['upload_path'] = './upload/news/';
    $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
    $config['max_size'] = '12048';  //12MB max
    $config['max_width'] = '4480'; // pixel
    $config['max_height'] = '4480'; // pixel
    $config['encrypt_name'] = TRUE;
    $picture = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0;$i<$cpt;$i++)
    {
      $_FILES['userfile']['name']= $files['userfile']['name'][$i];
      $_FILES['userfile']['type']= $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files['userfile']['error'][$i];
      $_FILES['userfile']['size']= $files['userfile']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $picture[] = $this->upload->data();
    }
    $data = array(
      'pic_title'     => $title,
      'pic_title1'     => $title1,
      'date'          => date('Y-m-d H:i:s'),
      'pic_file'      => $picture[0]['file_name'],
      'pic_file1'     => $picture[1]['file_name'],
      'pic_file2'     => $picture[2]['file_name'],
      'pic_file3'     => $picture[3]['file_name'],
      'pic_file4'     => $picture[4]['file_name'],
      'pic_category'     => $category,
      'pic_date_picker'      => $date_picker,
      'pic_project'     => $project,
      'pic_project1'     => $project1,
      'pic_desc'      => $desc,
      'pic_desc1'      => $desc1,
      'pic_slug'      => $slug,
    );

    $this->article_model->insert($data,$picture);
    redirect('backend/article',$data);

  }

  // delete
  public function deletedata($id,$picture)
  {
    $path = './upload/news/';
    @unlink($path.$picture);

    $id = array('pic_id' => $id );
    $this->article_model->delete($id);
    return redirect('backend/article',$data);
  }

  function delete_all(){
    // If record delete request is submitted
    if($this->input->post('bulk_delete_submit')){
      // Get all selected IDs
      $ids = $this->input->post('pic_id');

      // If id array is not empty
      if(!empty($ids)){
        // Delete records from the database
        $delete = $this->user->delete($ids);

        // If delete is successful
        if($delete){
          $data['statusMsg'] = 'Selected users have been deleted successfully.';
        }else{
          $data['statusMsg'] = 'Some problem occurred, please try again.';
        }
      }else{
        $data['statusMsg'] = 'Select at least 1 record to delete.';
      }
    }
  }
  // edit
  public function edit($id)
  {
    $kondisi = array('pic_id' => $id );

    $data['data'] = $this->article_model->get_by_id($kondisi);
    $data['title']   = 'Edit - Article';
    return $this->load->view('backend/content/article_edit',$data);
  }

  // update
  public function updatedata()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $id   = $this->input->post('pic_id');
    $title   = $this->input->post('pic_title');
    $title1   = $this->input->post('pic_title1');
    $category = $this->input->post('pic_category');
    $project = $this->input->post('pic_project');
    $project1 = $this->input->post('pic_project1');
    $desc = $this->input->post('pic_desc');
    $desc1 = $this->input->post('pic_desc1');
    $slug = slug($this->input->post('pic_title',TRUE));
    $kondisi = array('pic_id' => $id );
    $path = './upload/news/';

    $config['upload_path'] = './upload/news/';
    $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
    $config['max_size'] = '12048';  //12MB max
    $config['max_width'] = '4480'; // pixel
    $config['max_height'] = '4480'; // pixel
    $config['encrypt_name'] = TRUE;
    $picture = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0;$i<$cpt;$i++)
    {
      $_FILES['userfile']['name']= $files['userfile']['name'][$i];
      $_FILES['userfile']['type']= $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files['userfile']['error'][$i];
      $_FILES['userfile']['size']= $files['userfile']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $picture[] = $this->upload->data();
    }
    $data = array(
      'pic_title'     => $title,
      'pic_title1'     => $title1,
      'date'          => date('Y-m-d H:i:s'),
      'pic_file'      => $picture[0]['file_name'],
      'pic_file1'     => $picture[1]['file_name'],
      'pic_file2'     => $picture[2]['file_name'],
      'pic_file3'     => $picture[3]['file_name'],
      'pic_file4'     => $picture[4]['file_name'],
      'pic_category'     => $category,
      'pic_project'     => $project,
      'pic_project1'     => $project1,
      'pic_desc'      => $desc,
      'pic_desc1'      => $desc1,
      'pic_slug'      => $slug,
      'pic_date_picker'      => $date_picker,
    );
    // hapus foto pada direktori
    @unlink($path.$this->input->post('filelama'));
    // hapus foto pada direktori
    $this->article_model->update($data,$kondisi);
    return redirect('backend/article',$data); // end class
  }
}
