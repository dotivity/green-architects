<?php

class User extends CI_Controller {
  public $data         = array();
  public $page_config  = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->library('encrypt');
    $this->load->model('admin_model');
    $this->load->model('user_model');
    $this->load->model('info_model');
    $this->load->library('upload');
    $this->load->library('pagination');
    $this->user_model->check_role();

    if (!$this->session->userdata('cibb_user_id')) {
      redirect('gotobackend/login');
    }

    if ($this->session->userdata('admin_area') == 0) {
      redirect('backend/dashboard');
    }
  }

  function time_ago($timestamp){

    //type cast, current time, difference in timestamps
    $timestamp      = (int) $timestamp;
    $current_time   = time();
    $diff           = $current_time - $timestamp;

    //intervals in seconds
    $intervals      = array (
      'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );

    //now we just find the difference
    if ($diff == 0)
    {
      return 'just now';
    }
    if ($diff < 60)
    {
      return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
    }
    if ($diff >= 60 && $diff < $intervals['hour'])
    {
      $diff = floor($diff/$intervals['minute']);
      return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
    }
    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
      $diff = floor($diff/$intervals['hour']);
      return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
    }
    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
      $diff = floor($diff/$intervals['day']);
      return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
    }
    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
      $diff = floor($diff/$intervals['week']);
      return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
    }
    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
      $diff = floor($diff/$intervals['month']);
      return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
    }
    if ($diff >= $intervals['year'])
    {
      $diff = floor($diff/$intervals['year']);
      return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
    }
  }

  public function set_pagination()
  {
    $this->page_config['first_link']         = '&lsaquo; First';
    $this->page_config['first_tag_open']     = '<li>';
    $this->page_config['first_tag_close']    = '</li>';
    $this->page_config['last_link']          = 'Last &raquo;';
    $this->page_config['last_tag_open']      = '<li>';
    $this->page_config['last_tag_close']     = '</li>';
    $this->page_config['next_link']          = 'Next &rsaquo;';
    $this->page_config['next_tag_open']      = '<li>';
    $this->page_config['next_tag_close']     = '</li>';
    $this->page_config['prev_link']          = '&lsaquo; Prev';
    $this->page_config['prev_tag_open']      = '<li>';
    $this->page_config['prev_tag_close']     = '</li>';
    $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
    $this->page_config['cur_tag_close']      = '</a></li>';
    $this->page_config['num_tag_open']       = '<li>';
    $this->page_config['num_tag_close']      = '</li>';
  }

  public function index()
  {
    $data['user']= $this->admin_model->get_by_id($this->session->userdata('cibb_user_id'));
    $this->data['title']   = 'Administrator  ';
    $this->load->view('admin/header', $this->data);
    $this->load->view('admin/index');

  }

  // start user function
  public function user_view($user_id)
  {
    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // user updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // user deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }

    $this->db->order_by('username', 'asc');
    $this->data['users'] = $this->db->get('cibb_users')->result();
    $this->data['user']    = $this->db->get_where('cibb_users', array('id' => $user_id))->row();

    $this->data['title']   = 'List Account';
    $this->load->view('backend/user/html', $this->data);
    $this->load->view('backend/user/user_list');
  }


    public function user_add($user_id)
    {
      // event register button
      if ($this->input->post('btn-reg'))
      {
        $this->user_model->register();
        if ($this->user_model->error_count != 0) {
          $this->data['error']    = $this->user_model->error;
        } else {
          $this->session->set_userdata('tmp_success', 1);
          redirect('backend/user/user_add/'.$user_id);
        }
      }

      $tmp_success = $this->session->userdata('tmp_success');
      if ($tmp_success != NULL) {
        // new user created
        $this->session->unset_userdata('tmp_success');
        $this->data['tmp_success'] = 1;
      }

      // event login button
      if ($this->input->post('btn-login'))
      {
        $this->load->model('user_model');
        $this->user_model->check_login();
        if ($this->user_model->error_count != 0) {
          $this->data['error_login']    = $this->user_model->error;
        } else {
          redirect('user');
        }
      }
      $this->db->order_by('username', 'asc');
      $this->data['users'] = $this->db->get('cibb_users')->result();
      $this->data['user']    = $this->db->get_where('cibb_users', array('id' => $user_id))->row();
      $this->data['title']   = 'Add Account';
      $this->load->view('backend/user/html', $this->data);
      $this->load->view('backend/user/user_add');
    }

  public function user_detail($user_id)
  {
    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // user updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // user deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }

    $this->db->order_by('username', 'asc');
    $this->data['users'] = $this->db->get('cibb_users')->result();
    $this->data['user']    = $this->db->get_where('cibb_users', array('id' => $user_id))->row();
    $this->data['title']   = 'User Detail';
    $this->load->view('backend/user/html', $this->data);
    $this->load->view('backend/user/user_detail');
  }

  public function user_delete($user_id)
  {
    $this->db->delete('cibb_users', array('id' => $user_id));
    $this->session->set_userdata('tmp_success_del', 1);
    redirect('backend/user/user_view/'.$user_id);
  }

  public function user_edit($user_id)
  {
    if ($this->input->post('btn-save')) {

      $this->admin_model->user_edit();
      if ($this->admin_model->error_count != 0) {
        $this->data['error']    = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('backend/user/user_view/'.$user_id);
      }
    }

    $this->db->order_by('role', 'ASC');
    $this->data['roles']   = $this->db->get('cibb_roles')->result();
    $this->data['user']    = $this->db->get_where('cibb_users', array('id' => $user_id))->row();
    $this->data['title']   = 'Edit Account';

    $this->load->view('backend/user/html', $this->data);
    $this->load->view('backend/user/user_edit');
  }

  public function avatar_upload()
  {
    $config['upload_path']          = './uploads/avatar/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('avatar')){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('v_upload', $error);
		}else{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('v_upload_sukses', $data);
		}
	}


  // start roles function
  public function role_create()
  {
    if ($this->session->userdata('role_create') == 0) {
      redirect('admin');
    }
    if ($this->input->post('btn-create'))
    {
      $this->admin_model->role_create();
      if ($this->admin_model->error_count != 0) {
        $this->data['error']    = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('admin/role_create');
      }
    }

    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // new role created
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $this->data['title']   = 'Create New Role';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/role_create');
  }

  public function role_view()
  {
    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // role deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }

    $this->load->model('admin_model');
    $this->data['roles'] = $this->admin_model->role_get_all();
    $this->data['column_width'] = floor(100 / count($this->data['roles']));
    $this->data['title']   = 'List Of Roles';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/role_view');
  }

  public function role_edit($role_id)
  {
    if ($this->session->userdata('role_edit') == 0) {
      redirect('admin');
    }
    if ($this->input->post('btn-edit')) {
      $this->admin_model->role_edit();
      if ($this->admin_model->error_count != 0) {
        $this->data['error'] = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('admin/role_edit/'.$role_id);
      }
    }
    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // role updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }
    $this->data['role'] = $this->db->get_where('cibb_roles', array('id' => $role_id))->row();
    $this->data['title']   = 'Edit Role';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/role_edit');
  }

  public function role_delete($role_id)
  {
    if ($this->session->userdata('role_delete') == 0) {
      redirect('admin');
    }
    $this->db->delete('cibb_roles', array('id' => $role_id));
    $this->session->set_userdata('tmp_success_del', 1);
    redirect('admin/role_view');
  }
  // end roles function

  // start category function
  public function category_create()
  {
    if ($this->input->post('btn-create')) {
      $this->admin_model->category_create();
      if ($this->admin_model->error_count != 0) {
        $this->data['error']    = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('admin/category_create');
      }
    }

    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // new category created
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $this->data['categories'] = $this->admin_model->category_get_all();
    $this->data['title']   = 'Create Faculty';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/category_create');
  }

  public function category_view()
  {
    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // role deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }

    $this->data['categories'] = $this->admin_model->category_get_all();
    $this->data['title']   = 'List Of Faculty';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/category_view');
  }

  public function category_edit($category_id)
  {
    if ($this->input->post('btn-edit')) {
      $this->admin_model->category_edit();
      if ($this->admin_model->error_count != 0) {
        $this->data['error']    = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('admin/category_edit/'.$category_id);
      }
    }
    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // new category created
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }
    $this->data['category']   = $this->db->get_where('cibb_categories', array('id' => $category_id))->row();
    $this->data['categories'] = $this->admin_model->category_get_all();
    $this->data['title']   = 'Edit Faculty';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/category_edit');
  }

  public function category_delete($category_id)
  {
    $this->db->delete('cibb_categories', array('id' => $category_id));
    $this->session->set_userdata('tmp_success_del', 1);
    redirect('admin/category_view');
  }
  // end category function

  // start thread function
  public function thread_view($start = 0)
  {
    // set pagination
    $this->load->library('pagination');
    $this->page_config['base_url']    = site_url('admin/thread_view/');
    $this->page_config['uri_segment'] = 3;
    $this->page_config['total_rows']  = $this->db->count_all_results('cibb_threads');
    $this->page_config['per_page']    = 10;

    $this->set_pagination();

    $this->pagination->initialize($this->page_config);

    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // thread updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // thread deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }

    $this->data['start']   = $start;
    $this->data['page']    = $this->pagination->create_links();
    $this->data['threads'] = $this->admin_model->thread_get_all($start, $this->page_config['per_page']);
    $this->data['title']   = 'List Of Class';
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/thread_view');
  }

  public function thread_edit($thread_id)
  {
    if ($this->session->userdata('thread_edit') == 0) {
      redirect('admin');
    }
    if ($this->input->post('btn-save'))
    {
      $this->admin_model->thread_edit();
      if ($this->admin_model->error_count != 0) {
        $this->data['error']    = $this->admin_model->error;
      } else {
        $this->session->set_userdata('tmp_success', 1);
        redirect('admin/thread_view');
      }
    }
    $this->data['title']   = 'Edit Class';
    $this->data['thread']  = $this->db->get_where('cibb_threads', array('id' => $thread_id))->row();
    $this->data['categories'] = $this->admin_model->category_get_all();
    $this->load->view('admin/header', $this->data);

    $this->load->view('admin/thread_edit');
  }

  public function thread_delete($thread_id)
  {
    if ($this->session->userdata('thread_delete') == 0) {
      redirect('admin');
    }
    // delete thread
    $this->db->delete('cibb_threads', array('id' => $thread_id));

    // delete all posts on this thread
    $this->db->delete('cibb_posts', array('thread_id' => $thread_id));
    $this->session->set_userdata('tmp_success_del', 1);
    redirect('admin/thread_view');
  }
  // end thread function


  public function add()
  {
    $this->load->view('admin/information_create');

  }

  public function insertdata()

  {

    $id   = $this->input->post('id');
    $title   = $this->input->post('info_title');
    $slug = slug($this->input->post('info_title',TRUE));


    $config['upload_path'] = './uploads/files/';
    $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
    $config['max_size'] = '12048';  //12MB max
    $config['max_width'] = '4480'; // pixel
    $config['max_height'] = '4480'; // pixel
    $config['encrypt_name'] = TRUE;
    $picture = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0;$i<$cpt;$i++)
    {
      $_FILES['userfile']['name']= $files['userfile']['name'][$i];
      $_FILES['userfile']['type']= $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files['userfile']['error'][$i];
      $_FILES['userfile']['size']= $files['userfile']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $picture[] = $this->upload->data();
    }
    $data = array(
      'info_title'     => $title,
      'date'          => date('Y-m-d H:i:s'),
      'info_picture'      => $picture[0]['file_name'],
      'info_slug'     => $slug,
    );

    // hapus foto pada direktori
    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // new user created
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }
    $this->info_model->insert($data,$picture);
    redirect('information',$data);

  }

  // delete
  public function deletedata($id,$picture)
  {
    $path = './uploads/files/';
    @unlink($path.$picture);

    $where = array('id' => $id );
    $this->info_model->delete($where);
    return redirect('information');
  }

  // edit
  public function edit($id)
  {
    $kondisi = array('id' => $id );

    $data['data'] = $this->info_model->get_by_id($kondisi);
    return $this->load->view('admin/information_view',$data);
  }

  // update
  public function updatedata()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $id   = $this->input->post('id');
    $title   = $this->input->post('info_title');
    $slug = slug($this->input->post('info_title',TRUE));
    $path = './uploads/files/';
    $kondisi = array('id' => $id );


    $picture = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {
      $_FILES['userfile']['name']= $files['userfile']['name'][$i];
      $_FILES['userfile']['type']= $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files['userfile']['error'][$i];
      $_FILES['userfile']['size']= $files['userfile']['size'][$i];

      $config['upload_path'] = './uploads/files/';
      $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
      $config['max_size'] = '2048';  //2MB max
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['encrypt_name'] = TRUE;

      $this->upload->initialize($config);
      if (!empty($_FILES['userfile']['name'])) {
        if ( $this->upload->do_upload() ) {
          $picture[] = $this->upload->data();

          $data = array(
            'info_title'     => $title,
            'date'          => date('Y-m-d H:i:s'),
            'info_picture'      => $picture[0]['file_name'],
            'info_slug'     => $slug,
          );
          // hapus foto pada direktori
        }
      }
    }
    // hapus foto pada direktori
    @unlink($path.$this->input->post('filelama'));
    $this->info_model->update($data,$kondisi);
    return $this->load->view('admin/information_view');
  }

  public function information()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');

    $search = array('info_title' => $cari );

    $batas =  10; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_info/?cari='.$cari;
    $config['total_rows'] 			 = $this->info_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $data['data'] = $this->info_model->get($batas,$offset,$search);

    $this->load->view('admin/information_view',$data);
  }
}
