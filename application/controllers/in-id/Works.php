<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Works extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('portfolio_model');
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->library('pagination');


  }

  // fungsi untuk mengambil data
  public function index()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');

    $search = array('pic_title' => $cari );

    $batas =  3; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->portfolio_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;

    $this->db->order_by('date', 'desc');
    $data['portfolio'] = $this->portfolio_model->get($batas,$offset,$search);

    $data['title']   = 'Works - Green Architects';
    $this->load->view('frontend/id/works/index',$data);

  }
}
