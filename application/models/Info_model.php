<?php
/**
*
*/
class Info_model extends CI_Model
{

  function get($batas=NULL,$offset=NULL,$cari=NULL)
  {
    if ($batas != NULL) {
      $this->db->limit($batas,$offset);
    }
    if ($cari != NULL) {
      $this->db->or_like($cari);
    }
    $this->db->order_by('date','DESC');
    $this->db->from('cibb_informations');
    $query = $this->db->get();
    return $query->result();
  }
  function jumlah_row($search)
  {
    $this->db->or_like($search);
    $query = $this->db->get('cibb_informations');

    return $query->num_rows();
  }



  function get_by_id($kondisi)
  {
    $this->load->helper('url');
    $this->db->from('cibb_informations');
    $this->db->where($kondisi);
    $query = $this->db->get();
    return $query->row();

  }

  function get_by_title($kondisititle)
  {
    $this->db->from('cibb_informations');
    $this->db->where($kondisititle);
    $query = $this->db->get();
    return $query->result();
  }

  function insert($data)
  {
    $this->db->insert('cibb_informations',$data);
    return TRUE;
  }
  function delete($where)
  {
    $this->db->where($where);
    $this->db->delete('cibb_informations');
    return TRUE;
  }
  function update($data,$kondisi)
  {
    $this->db->update('cibb_informations',$data,$kondisi);
    return TRUE;
  }

}
