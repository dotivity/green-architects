<!-- Dashboard Analytics Start -->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
            <h2 class="content-header-title float-left mb-0">Site Map</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="content-body">

      <!-- Anchors and buttons start -->
      <section id="anchors-n-buttons">
        <div class="row match-height">


          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Front End | English</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?php echo site_url ('en-us')?>" target="_blank" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url ('en-us/about')?>" target="_blank" class="list-group-item list-group-item-action">About</a></li>
                    <a href="<?php echo site_url ('en-us/greetings')?>" target="_blank" class="list-group-item list-group-item-action">Greetings</a></li>
                    <a href="<?php echo site_url ('en-us/office')?>" target="_blank" class="list-group-item list-group-item-action">Our Office</a></li>
                    <a href="<?php echo site_url ('en-us/whatwedo')?>" target="_blank" class="list-group-item list-group-item-action">What We Do</a></li>
                    <a href="<?php echo site_url ('en-us/works')?>" target="_blank" class="list-group-item list-group-item-action">Works</a></li>
                    <a href="<?php echo site_url ('en-us/news')?>" target="_blank" class="list-group-item list-group-item-action">News</a></li>
                    <a href="<?php echo site_url ('en-us/careers')?>" target="_blank" class="list-group-item list-group-item-action">Careers</a></li>
                    <a href="<?php echo site_url ('en-us/contact')?>" target="_blank" class="list-group-item list-group-item-action">Contact</a></li>
                    <a href="<?php echo site_url ('error')?>" target="_blank" class="list-group-item list-group-item-action">404 Page</a>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Front End | Bahasa Indonesia</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?php echo site_url ('in-id')?>" target="_blank" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url ('in-id/about')?>" target="_blank" class="list-group-item list-group-item-action">Tentang Kami</a></li>
                    <a href="<?php echo site_url ('in-id/greetings')?>" target="_blank" class="list-group-item list-group-item-action">Sambutan</a></li>
                    <a href="<?php echo site_url ('in-id/office')?>" target="_blank" class="list-group-item list-group-item-action">Kantor Kami</a></li>
                    <a href="<?php echo site_url ('in-id/whatwedo')?>" target="_blank" class="list-group-item list-group-item-action">Apa Yang Kami Lakukan</a></li>
                    <a href="<?php echo site_url ('in-id/works')?>" target="_blank" class="list-group-item list-group-item-action">Pekerjaan</a></li>
                    <a href="<?php echo site_url ('in-id/news')?>" target="_blank" class="list-group-item list-group-item-action">Berita</a></li>
                    <a href="<?php echo site_url ('in-id/careers')?>" target="_blank" class="list-group-item list-group-item-action">Karir</a></li>
                    <a href="<?php echo site_url ('in-id/contact')?>" target="_blank" class="list-group-item list-group-item-action">Kontak</a></li>
                    <a href="<?php echo site_url ('error')?>" target="_blank" class="list-group-item list-group-item-action">404 Page</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Back End</h4>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="list-group">
                    <a href="<?=site_url('backend/dashboard')?>" class="list-group-item active">Home</a>
                    <a href="<?php echo site_url('backend/portfolio')?>" target="_blank" class="list-group-item list-group-item-action">Portfolio</a>
                    <a href="<?php echo site_url('backend/article')?>" target="_blank" class="list-group-item list-group-item-action">Article</a>
                    <?php if ($this->session->userdata('admin_area') != 0): ?>
                      <a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">My Account</a>
                      <a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">List Account</a>
                      <a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">Add Account</a>
                      <a href="<?php echo site_url('backend/user/edit_add/'.$this->session->userdata('cibb_user_id')); ?>" target="_blank" class="list-group-item list-group-item-action">Edit Account</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Anchors and buttons end -->

      <!-- Custom Listgroups start -->

      <!-- Custom Listgroups end -->


      <!-- Flush and Horizontal list group starts -->

      <!-- Flush and Horizontal list group Ends -->


      <!--List group with tabs Starts-->

      <!--List group with tabs Ends-->

    </div>
  </div>
</div>
<!-- Dashboard Analytics end -->

<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
