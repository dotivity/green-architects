

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/vendors.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/charts/apexcharts.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/extensions/tether-theme-arrows.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/extensions/tether.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/extensions/shepherd-theme-default.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/tables/ag-grid/ag-grid.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/tables/ag-grid/ag-theme-material.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/plugins/forms/validation/form-validation.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/forms/select/select2.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/file-uploaders/dropzone.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/tables/datatable/datatables.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/pickers/pickadate/pickadate.css')?>">

<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/bootstrap.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/bootstrap-extended.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/colors.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/components.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/themes/dark-layout.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/themes/semi-dark-layout.css')?>">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/core/menu/menu-types/vertical-menu.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/core/colors/palette-gradient.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/dashboard-analytics.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/card-analytics.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/plugins/tour/tour.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/app-user.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/aggrid.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/plugins/forms/wizard.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/plugins/file-uploaders/dropzone.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/data-list-view.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/quill/katex.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/quill/monokai-sublime.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/quill/quill.snow.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/quill/quill.bubble.css')?>">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/style.css')?>">
<!-- END: Custom CSS-->

<link rel="stylesheet" href="<?php echo base_url ('assets_backend/assets/css/bootstrap3-wysihtml5.min.css')?>">

</head>
<!-- END: Head-->
