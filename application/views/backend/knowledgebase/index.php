<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>Knowledge Base - Guidelines - Green Architects</title>
  <link rel="apple-touch-icon" href="<?php echo base_url ('assets_backend/app-assets/images/ico/apple-icon-120.png')?>">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url ('assets_frontend/images/favicon.ico')?>">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

  <!-- BEGIN: Vendor CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/vendors/css/vendors.min.css')?>">
  <!-- END: Vendor CSS-->

  <!-- BEGIN: Theme CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/bootstrap.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/bootstrap-extended.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/colors.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/components.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/themes/dark-layout.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/themes/semi-dark-layout.css')?>">

  <!-- BEGIN: Page CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/core/menu/menu-types/vertical-menu.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/core/colors/palette-gradient.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/app-assets/css/pages/faq.css')?>">
  <!-- END: Page CSS-->

  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_backend/assets/css/style.css')?>">

  <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">

  <!-- BEGIN: Header-->
  <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-container content">
        <div class="navbar-collapse" id="navbar-mobile">
          <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav">
              <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
              <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
              <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
              <!--     i.ficon.feather.icon-menu-->
              <li class="nav-item d-none d-lg-block"><h3 class="content-header-title float-left mb-0">Knowledge Base</h3></li>
            </ul>
            <ul class="nav navbar-nav">
            </ul>
          </div>
          <ul class="nav navbar-nav float-right">
            <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>

            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
              <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600"><?php echo $this->session->userdata('cibb_username'); ?></span><span class="user-status">Available</span></div><span><img class="round" src="<?php echo base_url ('assets_backend/app-assets/images/portrait/small/avatar.png')?>" alt="avatar" height="40" width="40"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="<?=site_url()?>gotobackend/logout"><i class="feather icon-power"></i> Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>


<ul class="main-search-list-defaultlist-other-list d-none">
  <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100 py-50">
    <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>No results found.</span></div>
  </a></li>
</ul>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">

        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>

    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url('backend/dashboard')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

      <?php if ($this->session->userdata('admin_area') != 0): ?>
        <li class=" navigation-header"><span>ACCOUNT</span>
        </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
          </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/client')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Client</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/publisher')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Publisher</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/promo')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Promo</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/campaign')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/testimonial')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Testimonial</span></a>
          </li>
        </ul>
      </li>

      <li class=" navigation-header"><span>GUIDELINES</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="User">Knowledge Base</span></a>
        <ul class="menu-content">
          <li class="active">
            <a href="<?php echo site_url('backend/knowledgebase')?>">
              <i class="feather icon-circle"></i>
              <span class="menu-item" data-i18n="View">
                Documentations
              </span>
            </a>
          </li>
        </ul>
      </li>
    </div>
  </div>

  <!-- END: Main Menu-->

  <!-- BEGIN: Content-->
  <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-body">
        <section id="faq-search">
          <div class="row">
            <div class="col-12">
              <div class="card faq-bg white">
                <div class="card-content">
                  <div class="card-body p-sm-4 p-2">
                    <h1 class="white">Search Guidelines ?</h1>

                    <form>
                      <fieldset class="form-group position-relative has-icon-left mb-0">
                        <input type="text" class="form-control form-control-lg" id="searchbar" placeholder="Search">
                        <div class="form-control-position">
                          <i class="feather icon-search px-1"></i>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="faq">
          <div class="row">
            <div class="col-xl-3 col-md-6 col-sm-12">
              <div class="card">
                <div class="card-content">
                  <img class="card-img-top img-fluid" src="<?php echo base_url ('assets_backend/documentation/images/template_campaign.png')?>" alt="Card image cap">
                  <div class="card-body">
                    <h5>Template Campaign</h5>
                    <p class="card-text  mb-0">Place at Our Portfolio</p>

                    <div class="card-btns d-flex justify-content-between mt-2">
                      <a href="<?php echo base_url ('assets_backend/documentation/images/template_campaign.png')?>" class="btn gradient-light-success text-white" download>Download</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-9 col-md-7 col-12">
              <div class="card bg-transparent border-0 shadow-none collapse-icon accordion-icon-rotate">
                <div class="card-body p-0">
                  <div class="accordion search-content-info" id="accordionExample">
                    <div class="collapse-margin search-content mt-0">
                      <div class="card-header" id="headingOne" role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        <span class="lead collapse-title">
                          Logo Client Guidelines
                        </span>
                      </div>
                      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                          - Canvas Size : 396px x 142px<br>
                          - Logo Size : Propotional Centered - 60-70%
                        </div>
                      </div>
                    </div>
                    <div class="collapse-margin">
                      <div class="card-header" id="headingTwo" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span class="lead collapse-title collapsed">
                          Logo Publisher Guidelines
                        </span>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                          - Canvas Size : 396px x 142px<br>
                          - Logo Size : Propotional Centered - 60-70%
                        </div>
                      </div>
                    </div>
                    <div class="collapse-margin search-content">
                      <div class="card-header" id="headingThree" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <span class="lead collapse-title collapsed">
                          Promo Guidelines
                        </span>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                          - Canvas Size : 1280px x 1280px
                        </div>
                      </div>
                    </div>
                    <div class="collapse-margin search-content">
                      <div class="card-header" id="headingFour" role="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <span class="lead collapse-title collapsed">
                          Portfolio Guidelines
                        </span>
                      </div>
                      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                          Download 'Phone Template Campaign' in the left side at Knowledge Base Menu
                          <br>
                          <br>
                          - Canvas Size : 640px x 480px<br>
                          - Phone Size : Height = 450px<br>
                          - Phone Size : Width = 225px<br>
                          - Capture Campaign = Propotional Centered with Phone
                        </div>
                      </div>
                    </div>
                    <div class="collapse-margin search-content">
                      <div class="card-header" id="headingFive" role="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <span class="lead collapse-title collapsed">
                          Testimonial Guidelines
                        </span>
                      </div>
                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">

                          - Canvas Size : 200px x 200px<br>
                          - Logo Size : Propotional Centered - 80%
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
  <!-- END: Content-->

  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>

  <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="<?php echo site_url ('backend/dashboard')?>" target="_blank">Green Architects,</a>All rights Reserved</span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i class="feather icon-heart pink"></i></span>
      <button class="btn btn-success btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
    </p>
  </footer>
  <!-- END: Footer-->



  <!-- BEGIN: Vendor JS-->
  <script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/vendors.min.js')?>"></script>
  <!-- BEGIN Vendor JS-->

  <!-- BEGIN: Page Vendor JS-->
  <!-- END: Page Vendor JS-->

  <!-- BEGIN: Theme JS-->
  <script src="<?php echo base_url ('assets_backend/app-assets/js/core/app-menu.js')?>"></script>
  <script src="<?php echo base_url ('assets_backend/app-assets/js/core/app.js')?>"></script>
  <script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/components.js')?>"></script>
  <!-- END: Theme JS-->

  <!-- BEGIN: Page JS-->
  <script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/pages/faq-kb.js')?>"></script>
  <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
