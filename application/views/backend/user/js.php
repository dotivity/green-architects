<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/vendors.min.js')?>"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/select/select2.full.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/pickers/pickadate/picker.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/pickers/pickadate/picker.date.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/extensions/jquery.steps.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/validation/jquery.validate.min.js')?>"></script>

<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/js/core/app-menu.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/core/app.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/components.js')?>"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/pages/app-user.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/navs/navs.js')?>"></script>
    <script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/forms/wizard-steps.js')?>"></script>
    <!-- END: Page JS-->

<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>

<script>

gapi.analytics.ready(function() {

  /**
   * Authorize the user immediately if the user has already granted access.
   * If no access has been created, render an authorize button inside the
   * element with the ID "embed-api-auth-container".
   */
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: '191300421'
  });


  /**
   * Create a ViewSelector for the first view to be rendered inside of an
   * element with the id "view-selector-1-container".
   */
  var viewSelector1 = new gapi.analytics.ViewSelector({
    container: 'view-selector-1-container'
  });

  /**
   * Create a ViewSelector for the second view to be rendered inside of an
   * element with the id "view-selector-2-container".
   */
  var viewSelector2 = new gapi.analytics.ViewSelector({
    container: 'view-selector-2-container'
  });

  // Render both view selectors to the page.
  viewSelector1.execute();
  viewSelector2.execute();


  /**
   * Create the first DataChart for top countries over the past 30 days.
   * It will be rendered inside an element with the id "chart-1-container".
   */
  var dataChart1 = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:country',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
      'max-results': 6,
      sort: '-ga:sessions'
    },
    chart: {
      container: 'chart-1-container',
      type: 'PIE',
      options: {
        width: '100%',
        pieHole: 4/9
      }
    }
  });


  /**
   * Create the second DataChart for top countries over the past 30 days.
   * It will be rendered inside an element with the id "chart-2-container".
   */
  var dataChart2 = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:country',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
      'max-results': 6,
      sort: '-ga:sessions'
    },
    chart: {
      container: 'chart-2-container',
      type: 'PIE',
      options: {
        width: '100%',
        pieHole: 4/9
      }
    }
  });

  /**
   * Update the first dataChart when the first view selecter is changed.
   */
  viewSelector1.on('change', function(ids) {
    dataChart1.set({query: {ids: ids}}).execute();
  });

  /**
   * Update the second dataChart when the second view selecter is changed.
   */
  viewSelector2.on('change', function(ids) {
    dataChart2.set({query: {ids: ids}}).execute();
  });

});
</script>
