

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<?php include 'header.php'; ?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">
        
        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>

    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url()?>backend/dashboard"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

      <?php if ($this->session->userdata('admin_area') != 0): ?>
        <li class=" navigation-header"><span>ACCOUNT</span>
        </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li  class="active"><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
          </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/client')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Client</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/publisher')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Publisher</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/promo')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Promo</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/campaign')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/testimonial')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Testimonial</span></a>
          </li>

        </ul>
      </li>

      <li class=" navigation-header"><span>GUIDELINES</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="User">Knowledge Base</span></a>
        <ul class="menu-content">
          <li>
            <a href="<?php echo site_url('backend/knowledgebase')?>">
              <i class="feather icon-circle"></i>
              <span class="menu-item" data-i18n="View">
                Documentations
              </span>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>


<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
          </div>
        </div>
      </div>
    </div>
    <div class="content-body">
      <?php if (isset($tmp_success)): ?>
        <div class="alert alert-success">
          <a class="close" data-dismiss="alert" href="#">&times;</a>
          <h4 class="alert-heading">User created!</h4>
        </div>
      <?php endif; ?>
      <?php if (isset($error)): ?>
        <div class="alert alert-error">
          <a class="close" data-dismiss="alert" href="#">&times;</a>
          <h4 class="alert-heading">Error!</h4>
          <?php if (isset($error['username'])): ?>
            <div>- <?php echo $error['username']; ?></div>
          <?php endif; ?>
          <?php if (isset($error['password'])): ?>
            <div>- <?php echo $error['password']; ?></div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <!-- users edit start -->
      <section class="multiple-validation">
        <div class="row match-height">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              </div>
              <div class="card-content">
                <div class="card-body">
                  <form class="form-horizontal"  action="" method="post" >
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-6 col-12">
                          <div class="form-label-group">
                            <input type="text" class="form-control" placeholder="Username" name="row[username]" required data-validation-required-message="This Username field is required" minlength="5">
                            <label for="first-name-column">Username</label>
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-label-group">
                            <input type="text" class="form-control" placeholder="Name" name="row[name]" required data-validation-required-message="This Name field is required">
                            <label for="name-column">Name</label>
                          </div>
                        </div>
                        <div class="col-md-12 col-12">
                          <div class="form-label-group">
                            <input type="email" class="form-control" name="row[email]" placeholder="Email" required data-validation-required-message="This Email field is required">
                            <label for="email-id-column">Email</label>
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-label-group">
                            <input type="password" class="form-control" placeholder="Password" name="row[password]" required data-validation-required-message="This Password field is required" minlength="5">
                            <label for="password-column">Password</label>
                          </div>
                        </div>
                        <div class="col-md-6 col-12">
                          <div class="form-label-group">
                            <input type="password" class="form-control" name="password2" placeholder="Confirm Password" required data-validation-required-message="This Confirm Password field is required" minlength="5">
                            <label for="confirm-password">Confirm Password</label>
                          </div>
                        </div>
                        <div class="col-12">
                          <input type="submit" name="btn-reg" class="btn btn-success mr-1 mb-1" value="Submit"/>
                          <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- users edit ends -->

    </div>
  </div>
</div>
<!-- END: Content-->

<?php
include 'footer.php';
include 'js.php';
include 'end.php';?>
