

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<?php include 'header.php'; ?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">

        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>

    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url()?>backend/dashboard"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

            <?php if ($this->session->userdata('admin_area') != 0): ?>
      <li class=" navigation-header"><span>ACCOUNT</span>
      </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li  class="active"><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
           </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/client')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Client</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/publisher')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Publisher</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/promo')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Promo</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/campaign')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/testimonial')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Testimonial</span></a>
          </li>

        </ul>
      </li>

      <li class=" navigation-header"><span>GUIDELINES</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="User">Knowledge Base</span></a>
        <ul class="menu-content">
          <li>
            <a href="<?php echo site_url('backend/knowledgebase')?>">
              <i class="feather icon-circle"></i>
              <span class="menu-item" data-i18n="View">
                Documentations
              </span>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>


<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- page users view start -->

            <section class="page-users-view">
                <div class="row">
                    <!-- account start -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Account</div>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                        <table>
                                            <tr>
                                                <td class="font-weight-bold">Username</td>
                                                <td><?php echo $user->username; ?> </td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold">Name</td>
                                                <td><?php echo $user->name; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold">Email</td>
                                                <td><?php echo $user->email; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-12 col-md-12 col-lg-5">
                                        <table class="ml-0 ml-sm-0 ml-lg-0">
                                            <tr>
                                                <td class="font-weight-bold">Role</td>
                                                <td><?php echo $user->roles; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold">Company</td>
                                                <td><?php echo $user->company; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-12">
                                        <a href="<?php echo site_url('backend/user/user_edit/'.$this->session->userdata('cibb_user_id')); ?>" class="btn btn-success mr-1"><i class="feather icon-edit-1"></i> Edit</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- account end -->
                    <!-- information start -->
                    <div class="col-md-6 col-12 ">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title mb-2">Information</div>
                            </div>
                            <div class="card-body">
                                <table>
                                    <tr>
                                        <td class="font-weight-bold">Mobile</td>
                                        <td><?php echo $user->mobile; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Website</td>
                                        <td><?php echo $user->website; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Gender</td>
                                        <td><?php echo $user->gender; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- information start -->
                    <!-- social links end -->
                    <div class="col-md-6 col-12 ">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title mb-2">Social Links</div>
                            </div>
                            <div class="card-body">
                                <table>
                                    <tr>
                                        <td class="font-weight-bold">Twitter</td>
                                        <td><?php echo $user->twitter; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Facebook</td>
                                        <td><?php echo $user->facebook; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Instagram</td>
                                        <td><?php echo $user->instagram; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">LinkedIn</td>
                                        <td><?php echo $user->linkedin; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- permissions end -->
                </div>
            </section>

            <!-- page users view end -->

        </div>
    </div>
</div>
<!-- END: Content-->

<?php
include 'footer.php';
include 'js.php';
include 'end.php';?>
