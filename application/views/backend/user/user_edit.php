

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<?php include 'header.php'; ?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">

        <h2 class="brand-text mb-0">Green Architects</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 success toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon success" data-ticon="icon-disc"></i></a></li>

    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url()?>backend/dashboard"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

            <?php if ($this->session->userdata('admin_area') != 0): ?>
      <li class=" navigation-header"><span>ACCOUNT</span>
      </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
           </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li><a href="<?php echo site_url('backend/client')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Client</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/publisher')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Logo Publisher</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/promo')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Promo</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/campaign')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>
          <li><a href="<?php echo site_url('backend/testimonial')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Testimonial</span></a>
          </li>

        </ul>
      </li>

      <li class=" navigation-header"><span>GUIDELINES</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-info"></i><span class="menu-title" data-i18n="User">Knowledge Base</span></a>
        <ul class="menu-content">
          <li>
            <a href="<?php echo site_url('backend/knowledgebase')?>">
              <i class="feather icon-circle"></i>
              <span class="menu-item" data-i18n="View">
                Documentations
              </span>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </div>
</div>


<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
      <!-- users edit start -->
      <section class="users-edit">
        <?php if (isset($error)): ?>
          <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <h4 class="alert-heading">Error!</h4>
            <?php if (isset($error['password'])): ?>
              <div>- <?php echo $error['password']; ?></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if (isset($tmp_success)): ?>
          <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <h4 class="alert-heading">Data Saved</h4>
          </div>
        <?php endif; ?>
        <div class="card">
          <form class="form-horizontal"  action="" method="post" >
            <div class="card-content">
              <div class="card-body">
                <ul class="nav nav-tabs mb-3" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                      <i class="feather icon-user mr-25"></i><span class="d-none d-sm-block">Account</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" id="password-tab" data-toggle="tab" href="#password" aria-controls="password" role="tab" aria-selected="false">
                      <i class="feather icon-share-2 mr-25"></i><span class="d-none d-sm-block">Password</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information" aria-controls="information" role="tab" aria-selected="false">
                      <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">Information</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link d-flex align-items-center" id="social-tab" data-toggle="tab" href="#social" aria-controls="social" role="tab" aria-selected="false">
                      <i class="feather icon-share-2 mr-25"></i><span class="d-none d-sm-block">Social</span>
                    </a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                    <!-- users edit media object start -->
                    <!-- users edit media object ends -->
                    <!-- users edit account form start -->


                    <div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="form-group">
                          <div class="controls">
                            <label>Username</label>
                            <input type="hidden" name="row[id]" value="<?php echo $user->id; ?>"/>
                            <input disabled="disabled" type="text" class="form-control" placeholder="Username" value="<?php echo $user->username; ?>" required data-validation-required-message="This username field is required">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="controls">
                            <label>Name</label>
                            <input type="text" name="row[name]" class="form-control" placeholder="Name" value="<?php echo $user->name; ?>" required data-validation-required-message="This name field is required">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="controls">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="row[email]" placeholder="Email" value="<?php echo $user->email; ?>" required data-validation-required-message="This email field is required">
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6">

                        <div class="form-group">
                          <label>Roles</label>
                          <select class="form-control" name="row[roles]">
                            <option value='<?php echo $user->roles?>' selected disabled hidden style="display: none"><?php echo $user->roles?></option>
                            <option value="Admin">Admin</option>
                            <option value="Staff">Staff</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Previlage</label>
                          <select class="form-control" name="row[role_id]">
                            <option value='<?php echo $user->role_id?>' selected disabled hidden style="display: none"><?php echo $user->roles?></option>
                            <option value="2">Admin</option>
                            <option value="3">Staff</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Company</label>
                          <input type="text" class="form-control" name="row[company]" placeholder="Company name" value="<?php echo $user->company; ?>">
                        </div>
                      </div>

                      <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                        <input name="btn-save" type="submit" class="btn btn-success glow mb-1 mb-sm-0 mr-0 mr-sm-1" value="Save Changes"/>
                        <button type="reset" class="btn btn-outline-warning">Reset</button>
                      </div>
                    </div>

                    <!-- users edit account form ends -->
                  </div>

                  <div class="tab-pane" id="password" aria-labelledby="password-tab" role="tabpanel">
                    <!-- users edit Info form start -->
                    <div class="row mt-1">
                      <div class="col-12 col-sm-6">
                        <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Edit Password</h5>
                        <div class="form-group">
                          <div class="controls">
                            <label>New Password</label>
                            <input type="password" name="row[password]" class="form-control">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="controls">
                            <label>Confirm Password</label>
                            <input type="password" name="row[password2]" class="form-control">
                          </div>
                        </div>
                      </div>

                      <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                        <input name="btn-save" type="submit" class="btn btn-success glow mb-1 mb-sm-0 mr-0 mr-sm-1" value="Save Changes"/>
                        <button type="reset" class="btn btn-outline-warning">Reset</button>
                      </div>
                    </div>
                    <!-- users edit Info form ends -->
                  </div>

                  <div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
                    <!-- users edit Info form start -->
                    <div class="row mt-1">
                      <div class="col-12 col-sm-6">
                        <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Personal Information</h5>
                        <div class="form-group">
                          <div class="controls">
                            <label>Mobile</label>
                            <input type="text" name="row[mobile]" class="form-control" value="<?php echo $user->mobile; ?>" placeholder="Mobile number here...">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="controls">
                            <label>Website</label>
                            <input type="text" name="row[website]" class="form-control" value="<?php echo $user->website; ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Gender</label>
                          <ul class="list-unstyled mb-0">
                            <li class="d-inline-block mr-2" >
                              <fieldset>
                                <div class="vs-radio-con">
                                  <input type="radio" name="row[gender]" value="Male">
                                  <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                  </span>
                                  Male
                                </div>
                              </fieldset>
                            </li>
                            <li class="d-inline-block mr-2">
                              <fieldset>
                                <div class="vs-radio-con">
                                  <input type="radio" name="row[gender]" value="Female">
                                  <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                  </span>
                                  Female
                                </div>
                              </fieldset>
                            </li>

                          </ul>
                        </div>

                      </div>

                      <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                        <input name="btn-save" type="submit" class="btn btn-success glow mb-1 mb-sm-0 mr-0 mr-sm-1" value="Save Changes"/>
                        <button type="reset" class="btn btn-outline-warning">Reset</button>
                      </div>
                    </div>
                    <!-- users edit Info form ends -->
                  </div>
                  <div class="tab-pane" id="social" aria-labelledby="social-tab" role="tabpanel">
                    <!-- users edit socail form start -->
                    <div class="row">
                      <div class="col-12 col-sm-6">

                        <fieldset>
                          <label>Twitter</label>
                          <div class="input-group mb-75">
                            <div class="input-group-prepend">
                              <span class="input-group-text feather icon-twitter" id="basic-addon3"></span>
                            </div>
                            <input type="text" name="row[facebook]" class="form-control" value="<?php echo $user->twitter; ?>" placeholder="https://www.twitter.com/" aria-describedby="basic-addon3">
                          </div>

                          <label>Facebook</label>
                          <div class="input-group mb-75">
                            <div class="input-group-prepend">
                              <span class="input-group-text feather icon-facebook" id="basic-addon4"></span>
                            </div>
                            <input type="text" name="row[instagram]" class="form-control" value="<?php echo $user->facebook; ?>" placeholder="https://www.facebook.com/" aria-describedby="basic-addon4">
                          </div>

                        </fieldset>
                      </div>
                      <div class="col-12 col-sm-6">
                        <label>Instagram</label>
                        <div class="input-group mb-75">
                          <div class="input-group-prepend">
                            <span class="input-group-text feather icon-instagram" id="basic-addon5"></span>
                          </div>
                          <input type="text" name="row[twitter]" class="form-control" value="<?php echo $user->instagram; ?>" placeholder="https://www.instagram.com/" aria-describedby="basic-addon5">
                        </div>
                        <label>LinkedIn</label>
                        <div class="input-group mb-75">
                          <div class="input-group-prepend">
                            <span class="input-group-text feather icon-linkedin" id="basic-addon9"></span>
                          </div>
                          <input type="text" name="row[linkedin]" class="form-control" value="<?php echo $user->linkedin; ?>" placeholder="https://www.linkedin.com/" aria-describedby="basic-addon9">
                        </div>

                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                          <input name="btn-save" type="submit" class="btn btn-success glow mb-1 mb-sm-0 mr-0 mr-sm-1" value="Save Changes"/>
                          <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </div>
                      </div>
                      <!-- users edit socail form ends -->
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </form>
        </section>
        <!-- users edit ends -->
      </div>
    </div>
  </div>
</div>
<!-- END: Content-->

<?php
include 'footer.php';
include 'js.php';
include 'end.php';?>
