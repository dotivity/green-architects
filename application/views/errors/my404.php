<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS" />
  <meta name="description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design.">
  <meta name="keywords" content=" GREEN ARCHITECTS - Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS ">
  <meta name="author" content=" GREEN ARCHITECTS ">
  <meta name="identifier-URL" content="https://www.bygreenarchitects.com">
  <meta property="og:title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS"/>
  <meta property="og:url" content="https:// www.bygreenarchitects.com "/>
  <meta property="og:type" content="website" />
  <meta property="og:description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design."/>
  <meta property="og:image" content="<?php echo base_url ('assets_frontend/meta/meta-home.jpg')?>">
  <meta property="og:site_name" content=" Jasa Arsitek Jakarta – GREEN ARCHITECTS"/>
  <title>404 Not Found = Green Architects</title>
  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend_comingsoon/images/ico/favicon.ico')?>" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_frontend_comingsoon/css/asf2525xljt.css')?>"/>
  <!-- Global site tag (gtag.js) - Google Analytics -->

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180904228-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-180904228-1');
  </script>

</head>
<body>
  <div id="main">
    <div id="page-loader">
      <div class="spinner-container"><div class="css-spinner"></div></div>
    </div>
    <section id="hero" class="hero hero-1">
      <div class="front-content page-enter-animated">
        <div class="container-mid">
          <a href="<?php echo site_url ('')?>">
            <img class="logo" src="<?php echo base_url ('assets_frontend_comingsoon/images/logo1.png')?>" alt="logo" />
          </a>
          <div class="cycle-wrapper">

                <h1 style="color:#AAC71B">
                  404 NOT FOUND
                </h1>

          </div>
        </div>
      </div>
      <div class="background-content page-enter-animated">
        <div class="level-1">
          <video style="width:100%;height:100%" poster="<?php echo base_url ('assets_frontend/img/home/green-architects-header.png')?>" autoplay loop playsinline muted>
            <source src="<?php echo base_url ('assets_frontend/video/green-architects-video.mp4')?>" type="video/mp4">
              <source src="<?php echo base_url ('assets_frontend/video/green-architects-video.webm')?>" type="video/webm">
                Your browser does not support HTML5 video.
              </video>
          <div id="canvas"><canvas class="bg-effect layer" data-depth="0.2"></canvas></div>
        </div>
      </div>
    </section>
  </div>
  <script type="text/javascript" src="<?php echo base_url ('assets_frontend_comingsoon/js/xsg235t4r1r.js')?>"></script>
</body>
</html>
