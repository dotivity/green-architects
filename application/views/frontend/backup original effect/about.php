<div class="wrapper">

  <section class="intro-about section-margin">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="intro-content-text">

            <h2 data-dsn-grid="move-section" data-dsn-move="-30" data-dsn-duration="100%"
            data-dsn-opacity="1.2" data-dsn-responsive="tablet">
            Hello we are, <br /> Green Architects
          </h2>

          <p data-dsn-animate="text">Our brand identity is all about a commitment to making buildings that serve for meaningful social and urban change. <br>How Green Architects is a touchpoint in people’s real lives. We’re here to help them navigate their lives, satisfy their cravings, explore their interests, and expand their horizons.</p>

          <h6 data-dsn-animate="text">Liverpool, Northwest England</h6>

          <div class="exper">
            <div class="numb-ex">
              <span class="word" data-dsn-animate="text">2018</span>
            </div>

            <h4 data-dsn-animate="up">GREEN ARCHITECTS <br> WAS FOUNDED</h4>
          </div>

        </div>
      </div>
    </div>
  </div>


  <div class="background-mask">
    <div class="background-mask-bg"></div>
    <div class="img-box">
      <div class="img-cent" data-dsn-grid="move-up">
        <div class="img-container">
          <img data-dsn-y="30%" src="<?php echo base_url ('assets_frontend/img/home/about_us1.jpg')?>" alt="">
        </div>
      </div>
    </div>
  </div>
</section>

<div class="wrapper">
  <div class="root-about">
    <div class="box-seat box-seat-full">
      <div class="container-fluid">
        <div class="inner-img" data-dsn-grid="move-up">
          <img src="<?php echo base_url ('assets_frontend/img/home/about_us2.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
        </div>
        <div class="pro-text">
          <p data-dsn-animate="up">
            We also have become an interdisciplinary design firm with a collective of architects, interior designers, landscape designers, researchers, and urban designers. <br><br>
            We have experts from across the world such as the United Kingdom, India, Mexico, and Saudi Arabia who are committed to promoting green design. With a diverse workforce, we believe that we can provide the best solutions for a more resilient future. <br><br>
          </p>
        </div>
      </div>
    </div>


    <div class="box-seat-v1 box-seat-full">
      <div class="container-fluid">
        <div class="pro-text" style="z-index:10">
          <p data-dsn-animate="up">
            Focusing on eco-friendly design is our utmost importance. Our company therefore uses recent technologies and tools to develop and tests our sustainable designs. <br><br>
            This is what our branding is all about - connecting, serving as a touchpoint, and our responsibility as architects to hold the hands of people and our partners in order to create a better world.
          </p>
        </div>
        <div class="inner-img-v1" data-dsn-grid="move-up">
          <img src="<?php echo base_url ('assets_frontend/img/home/about_us3.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
        </div>
      </div>
    </div>
  </div>
</div>
