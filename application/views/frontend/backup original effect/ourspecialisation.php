<div class="box-gallery-vertical box-gallery-vertical-order section-margin section-padding">
    <div class="mask-bg"></div>
    <div class="container">
        <div class="row align-items-center h-100">
            <div class="col-lg-6">
                <div class="box-im" data-dsn-grid="move-up">
                    <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/our_specialisation.jpg')?>" alt=""
                        data-dsn-move="20%">
                </div>
            </div>

            <div class="col-lg-6">


                <div class="box-info">

                    <div class="vertical-title" data-dsn-animate="up">
                        <h2>Our Specialisation</h2>
                    </div>

                    <h6 data-dsn-animate="up">Our top priority is on the Zero/low carbon buildings approach. </h6>

                    <p data-dsn-animate="up">However, in our home, defining sustainable and modern design in Indonesia that reflect or provide for our culture is also as important. Mixing elements to create something fresh and customised to the character of the people and place.</p>

                </div>
            </div>

        </div>
    </div>
</div>
