<section class="our-team our-team-not-bg section-margin section-padding">
    <div class="container">
        <div class="one-title">
            <div class="title-sub-container">
                <p class="title-sub">our team</p>
            </div>
            <h2 class="title-main">Partnership</h2>
        </div>

        <div class="custom-container">
            <div class="slick-slider">
                <div class="team-item slick-slide">
                    <div class="box-img">
                        <img src="assets/img/team/1.png" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Berry Natalegawa</h4>
                        <p>United Kingdom</p>
                    </div>
                </div>

                <div class="team-item slick-slide">
                    <div class="box-img">
                        <img src="assets/img/team/2.png" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Saima Mirza</h4>
                        <p>India</p>
                    </div>
                </div>

                <div class="team-item slick-slide">
                    <div class="box-img">
                        <img src="assets/img/team/3.png" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Carlos Aguirre</h4>
                        <p>Mexico</p>
                    </div>
                </div>

                <div class="team-item slick-slide">
                    <div class="box-img">
                        <img src="assets/img/team/4.png" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Mohammed Alharthi</h4>
                        <p>Saudi Arabia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
