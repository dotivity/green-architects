<section class="our-work work-under-header  section-margin" data-dsn-col="3">
  <div class="container">
    <div class="one-title">
      <div class="title-sub-container">
        <p class="title-sub">Our Work</p>
      </div>
      <h2 class="title-main">Creative Portfolio Designs</h2>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 offset-lg-3">
        <div class="work-container">
          <div class="slick-slider">
            <div class="work-item slick-slide">
              <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/portfolio_temp/Climate_sensitive_Design.jpg')?>" alt="">
              <div class="item-border"></div>
              <div class="item-info">
                <a href="#" data-dsn-grid="move-up" class="effect-ajax">

                  <h5 class="cat">-</h5>
                  <h4>Shipping Container House</h4>
                  <span><span>A shipping container house for a student located in Birmingham, UK. Designed in 2019 when the use of shipping container was popular and considered to be sustainable.</span></span>
                </a>

              </div>
            </div>

            <div class="work-item slick-slide">
              <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/portfolio_temp/Cafe_1.jpg')?>" alt="">
              <div class="item-border"></div>
              <div class="item-info">
                <a href="#" data-dsn-grid="move-up" class="effect-ajax">

                  <h5 class="cat">-</h5>
                  <h4>Zero Energy Cafe in Dhaka, Bangladesh.</h4>
                  <span><span>Green Architects aim to build a zero energy house every year for the low-middle class as our corporate responsibility. As architects who build grand places and luxury projects, we want to spare our time and energy to design humble houses with low carbon footprint materials. This house aims to introduce the “kampung” community to how architecture can influence people’s wellbeing, this house will soon become a model where a small community can learn and apply the principles of sustainable design in their future home.</span></span>
                </a>

              </div>
            </div>

            <div class="work-item slick-slide">
              <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/portfolio_temp/Eco_1.jpg')?>" alt="">
              <div class="item-border"></div>
              <div class="item-info">
                <a href="#" data-dsn-grid="move-up" class="effect-ajax">

                  <h5 class="cat">-</h5>
                  <h4>Eco House, South Jakarta</h4>
                  <span><span>Incoming Description</span></span>
                </a>

              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</section>
