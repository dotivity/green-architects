<section class="client-see section-margin">
    <div class="container">
        <div class="inner">
            <div class="left">
                <h2 class="title" data-dsn-grid="move-section" data-dsn-move="-60"
                    data-dsn-duration="100%" data-dsn-opacity="1" data-dsn-responsive="tablet">
                    <span class="text">Our Vision</span>
                </h2>
            </div>

            <div class="items">
                <div class="bg"></div>
                <div class="">
                    <div class="item">
                        <div class="quote">
                            <p>Building a sustainable Indonesia with a worldwide perspective and as a leading firm in Southeast Asia.</p>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>
