<div class="box-seat">
    <div class="dsn-v-text">
        <div class="container-fluid">
            <div class="box-middle-text">
                <h2 data-dsn-animate="text">What We Do</h2>
                <p data-dsn-animate="text">We are shaping and supporting positive human behaviour through architecture design, interior, landscape, and urban design. We specialise in sustainable residential design to high rise design for all over Asia. Through technologies, we will take you to find your own definitions of what it means to be sustainable.</p>
            </div>
            <div class="inner-img" data-dsn-grid="move-up" data-overlay="9">
                <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/what_we_do.jpg')?>" alt="">
            </div>
        </div>
    </div>
</div>
