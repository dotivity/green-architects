<section class="our-services-2 section-margin">
    <div class="container">
        <div class="one-title" data-dsn-animate="up">
            <div class="title-sub-container">
                <p class="title-sub">Our</p>
            </div>
            <h2 class="title-main">Corporate Values</h2>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="services-item">
                    <div class="corner corner-top"></div>
                    <div class="corner corner-bottom"></div>
                    <div class="icon">
                        <img src="<?php echo base_url ('assets_frontend/img/corporate_value/integrity.png')?>" alt="">
                    </div>
                    <div class="services-header">
                        <h3>INTEGRITY</h3>
                    </div>
                    <p>We ensure all processes are carried out transparently.</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="services-item selected">
                    <div class="corner corner-top"></div>
                    <div class="corner corner-bottom"></div>
                    <div class="icon">
                        <img src="<?php echo base_url ('assets_frontend/img/corporate_value/speed.png')?>" alt="">
                    </div>
                    <div class="services-header">
                        <h3>SPEED</h3>
                    </div>
                    <p>We always adapt to the latest concepts and technology developments.</p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="services-item">
                    <div class="corner corner-top"></div>
                    <div class="corner corner-bottom"></div>
                    <div class="icon">
                        <img src="<?php echo base_url ('assets_frontend/img/corporate_value/professionalism.png')?>" alt="">
                    </div>
                    <div class="services-header">
                        <h3>PROFESSIONALISM</h3>
                    </div>
                    <p>We carry out detailed planning, effective and efficient execution, and make continuous improvement.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="services-item">
                    <div class="corner corner-top"></div>
                    <div class="corner corner-bottom"></div>
                    <div class="icon">
                        <img src="<?php echo base_url ('assets_frontend/img/corporate_value/customer_oriented.png')?>" alt="">
                    </div>
                    <div class="services-header">
                        <h3>CUSTOMER ORIENTED</h3>
                    </div>
                    <p>Focus on customer interests, and adjust preferences according to customer needs.</p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="services-item">
                    <div class="corner corner-top"></div>
                    <div class="corner corner-bottom"></div>
                    <div class="icon">
                        <img src="<?php echo base_url ('assets_frontend/img/corporate_value/caring.png')?>" alt="">
                    </div>
                    <div class="services-header">
                        <h3>CARING</h3>
                    </div>
                    <p>We believe that the impact of sustainable design will return to the wider community, namely by making costs efficient, companies or individuals can allocate these funds to help the surrounding community.</p>
                </div>
            </div>
            </div>
    </div>
</section>
