<footer class="footer">
  <div class="container">
    <div class="footer-links p-relative">
      <div class="row">
        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block">
            <div class="footer-logo">
              <a href="#"><img src="assets/img/logo-dark.png" alt=""></a>
            </div>

            <div class="footer-social">

              <ul>
                <li><a href="https://www.linkedin.com/company/bygreenarchitects"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.instagram.com/bygreenarchitects/"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>

              </ul>

            </div>
          </div>
        </div>

        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block col-menu">
            <h4 class="footer-title">Navigation</h4>
            <nav>
              <ul>
                <li><a href="#">Portfolio</a>
                </li>
                <li><a href="#">About</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Contact</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>


        <div class="col-md-3 dsn-col-footer">
          <div class="col-address">
            <h4 class="footer-title">Address</h4>

            <p>Menara 165, Lantai 2<br>
              Jl. TB. Simatupang Kav. 1<br>
              Cilandak Timur, Pasar Minggu<br>
              Jakarta Selatan (12560)</p>
            </div>
          </div>


          <div class="col-md-3 dsn-col-footer">
            <div class="footer-block col-contact">
              <h4 class="footer-title">Contact</h4>
              <p class="over-hidden"><strong>E</strong> <span>:</span><a class="link-hover"
                data-hover-text="info@bygreenarchitects.com" href="mailto:info@bygreenarchitects.com">info@bygreenarchitects.com</a>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="copyright">
        <div class="text-center">
          <p>© 2020 By Green Architects</p>
        </div>
      </div>
    </div>
  </footer>
</div>
</main>
