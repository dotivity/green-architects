<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="title" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser">
  	<meta name="description" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser">
  	<meta nam e="keywords" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser">
  	<meta name="author" content="By Green Architects">
  	<meta name="identifier-URL" content="https://www.bygreenarchitects.com/">
  	<meta property="og:title" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser">
  	<meta property="og:url" content="https://www.bygreenarchitects.com"/>
  	<meta property="og:type" content="website" />
  	<meta property="og:description" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser">
  	<meta property="og:image" content="<?php echo base_url ('assets_frontend/meta/meta-home.jpg')?>">
  	<meta property="og:site_name" content="Jasa Laser Cutting, Marking, Engraving murah di jakarta, Acrylic Display, Pembuatan Neon Box, Design Logo | GTT Laser"/>

    <!--  Title -->
    <title>By Green Architects</title>

    <!-- Font Google -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&amp;display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />

    <!-- custom styles (optional) -->
    <link href="<?php echo base_url ('assets_frontend/css/plugins.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url ('assets_frontend/css/style.css')?>" rel="stylesheet" />
</head>
