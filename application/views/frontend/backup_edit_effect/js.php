<!-- Optional JavaScript -->
<script src="<?php echo base_url ('assets_frontend/js/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/plugins.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/modernizr.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/dsn-grid.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/custom.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/skrollr.min.js')?>"></script>
<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2H49179QuTkfPvnbb7BpKpqA40IcDCpI&map_ids=7688f6a0d0448571&callback=initMap">
</script>
<script type="text/javascript">
map = new google.maps.Map(document.getElementById('map'), {
  center: {lat: -6.290354539788058, lng: 106.80960655532841},
  zoom: 8,
  mapId: '7688f6a0d0448571'
});
</script>
<script type="text/javascript">
$(function () {
  // Init function
  function skrollrInit() {
    skrollr.init();
  }

  // If window width is large enough, initialize skrollr
  if ($(window).width() > 767) {
    skrollrInit();
  }

  // On resize, check window width, if too small
  // and skrollr instance exists, destroy;
  // Otherwise, if window is large enough
  // and skrollr instance does not exist, initialize skrollr.
  $(window).on('resize', function () {
    var _skrollr = skrollr.get(); // get() returns the skrollr instance or undefined
    var windowWidth = $(window).width();

    if ( windowWidth <= 767 && _skrollr !== undefined ) {
      _skrollr.destroy();
    } else if ( windowWidth > 767 && _skrollr === undefined ) {
      skrollrInit();
    }
  });
});
</script>
