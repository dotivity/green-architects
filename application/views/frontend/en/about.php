<div class="wrapper">


  <section class="intro-about section-margin">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="intro-content-text">

            <h2 data-bottom="opacity: 0;margin-right:150px" data-200-center="opacity:1;margin-right:0px">
              Hello we are, <br /> Green Architects
            </h2>

            <p data-dsn-animate="text" data-bottom="opacity: 0;" data-200-center="opacity:1;">
              We are the <b>leading</b> and the most influential design firm that commits to making low-zero carbon buildings that serve for meaningful social and urban change.<br>
            </p>
            <p data-dsn-animate="text" data-bottom="opacity: 0;" data-200-center="opacity:1;">
              As Indonesia is vulnerable to <b>climate change</b>, we are here to increase your quality of life, save your energy bills, and increase your property value. <br>
            </p>
            <p data-dsn-animate="text" data-bottom="opacity: 0;" data-200-center="opacity:1;">
              We also have become an <b>interdisciplinary</b> design firm with a collective of architects, interior designers, landscape designers, researchers, urban designers, and contractors.
            </p>

            <h6 data-dsn-animate="text"  data-bottom="opacity: 0;margin-left:240px" data-200-center="opacity:1;margin-left:0px">Liverpool, Northwest England</h6>

            <div class="exper" data-bottom="opacity: 0;margin-left:200px" data-200-center-top="opacity:1;margin-left:0px">
              <div class="numb-ex">
                <span class="word" data-dsn-animate="text">2018</span>
              </div>

              <h4 data-dsn-animate="up">GREEN ARCHITECTS <br> WAS FOUNDED</h4>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="background-mask">
      <div class="background-mask-bg-droow"></div>

      <div class="img-box">
        <div class="img-cent" data-dsn-grid="move-up">
          <div class="svg-wrapper-custom" >
            <svg height="100%" width="100%" xmlns="http://www.w3.org/2000/svg">
              <rect id="route-1" class="shape-custom" height="100%" width="100%" />
            </svg>
          </div>
          <div  id="trigger-route-1" class="img-container">
            <img src="<?php echo base_url ('assets_frontend/img/home/about_us1.jpg')?>" alt="">
          </div>
        </div>
      </div>



    </div>
  </section>



  <div class="wrapper">
    <div class="root-about">
      <div class="box-seat box-seat-full">
        <div class="container-fluid">
          <div class="letter-effect" data-bottom="opacity: 0;margin-right:500px" data-200-center="opacity:1;margin-right:200px">Green</div>

          <div class="inner-img" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/about_us2.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
          <div class="pro-text">
            <p data-bottom="opacity: 0;" data-center="opacity:1;">
              To give the best service and result we use <b>western standard</b> design solutions and recent technologies. Also, in this digitally transformed world, we invest in our company’s values and vision to become more adept to change. <br><br>
              We have experts from across the world such as the United Kingdom, India, Mexico, and Saudi Arabia who are committed to promoting green design. With a <b>diverse workforce</b>, we believe that we can provide the best solutions for a more resilient future.<br>
            </p>
          </div>
        </div>
      </div>


      <div class="box-seat-v1 box-seat-full">
        <div class="container-fluid">
          <div class="pro-text" style="z-index:10">
            <p  data-bottom="opacity: 0" data-center="opacity:1">
              Focusing on eco-friendly design is our utmost importance. By choosing to design and build with us, you are contributing to promoting a <b>safe working place</b>  and helping us on tackling one of the biggest issues Indonesia faces that is uncertified construction workers. Our contractors will ensure that the building is built to a sustainable standard.<br><br>
              This is what our branding is all about, to hold the hands of people and our partners in order to create a better world.
            </p>
          </div>
          <div class="letter-effect-2" data-bottom="opacity: 0;margin-left:500px" data-200-center="opacity:1;margin-left:200px">Architects</div>
          <div class="inner-img-v1" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/about_us3.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
        </div>
      </div>
    </div>
  </div>
