<div class="container">
  <div class="one-title">
    <div class="title-sub-container">
      <p class="title-sub">Our</p>
    </div>
    <h2 class="title-main">Corporate Values</h2>
  </div>
</div>

<div class="embed-responsive embed-responsive-21by9">
  <iframe id="preview-frame-corporatevalue" class="embed-responsive-item" src="https://corporatevalue.bygreenarchitects.com/"></iframe>
</div>
