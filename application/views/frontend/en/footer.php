

<footer class="footer section-margin-top">
  <div class="container">
    <div class="footer-links p-relative">
      <div class="row">
        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block">
            <div class="footer-logo">
              <a href="<?php echo site_url ('en-us')?>"><img src="<?php echo base_url ('assets_frontend/img/logo-dark.png')?>" alt=""></a>
            </div>

            <div class="footer-social">

              <ul>
                <li><a href="https://www.linkedin.com/company/bygreenarchitects" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.instagram.com/bygreenarchitects/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/bygreenarchitects" target="_blank"><i class="fab fa-facebook"></i></a></li>

              </ul>

            </div>
          </div>
        </div>

        <div class="col-md-3 dsn-col-footer">
          <div class="footer-block col-menu">
            <h4 class="footer-title">Navigation</h4>
            <nav>
              <ul>
                <li><a href="<?php echo site_url ('en-us')?>">Home</a>
                </li>
                <li><a href="<?php echo site_url ('en-us/about')?>">About</a></li>
                <li><a href="<?php echo site_url ('en-us/greetings')?>">Greetings</a></li>
                <li><a href="<?php echo site_url ('en-us/office')?>">Our Office</a></li>
                <li><a href="<?php echo site_url ('en-us/whatwedo')?>">What We Do</a></li>
                <li><a href="<?php echo site_url ('en-us/works')?>">Works</a></li>
                <li><a href="<?php echo site_url ('en-us/news')?>">News</a></li>
                <li><a href="<?php echo site_url ('en-us/careers')?>">Careers</a></li>
                <li><a href="<?php echo site_url ('en-us/contact')?>">Contact</a></li>
              </ul>
            </nav>
          </div>
        </div>


        <div class="col-md-3 dsn-col-footer">
          <div class="col-address">
            <h4 class="footer-title">Address</h4>

            <p>MENARA 165, 4TH FLOOR<br>
              TB. SIMATUPANG STREET, KAV. 1<br>
              CILANDAK TIMUR, PASAR MINGGU<br>
              JAKARTA SELATAN (12560)</p>
            </div>
          </div>


          <div class="col-md-3 dsn-col-footer">
            <div class="footer-block col-contact">
              <h4 class="footer-title">Contact</h4>
              <p class="over-hidden"><strong><i class="fas fa-envelope-square"></i></strong> <span>:</span><a class="link-hover"
                data-hover-text="info@bygreenarchitects.com" href="mailto:info@bygreenarchitects.com">info@bygreenarchitects.com</a>
              </p>
              <p class="over-hidden"><strong><i class="fab fa-whatsapp"></i></strong> <span>:</span><a class="link-hover"
                data-hover-text="info@bygreenarchitects.com" href="https://wa.me/628111135601">+62-8111-1356-01</a>
              </p>
            </div>
          </div>
        </div>
      </div>

    </div>


    <div class="copyright">
      <div class="text-center">
        <p>© 2021 By Green Architects</p>
      </div>
    </div>
  </div>
</footer>
</div>
</main>
