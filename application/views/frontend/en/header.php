<main class="main-root">

  <div id="dsn-scrollbar">
    <div class="dsn-slider demo3" data-dsn-header="project" style="z-index: 990";> <div class="dsn-root-slider" id="dsn-hero-parallax-img">
      <div class="slide-inner">
        <div class="swiper-wrapper">

          <div class="slide-item swiper-slide">
            <div class="slide-content">
              <div class="slide-content-inner">
                <div class="project-metas">
                  <div class="project-meta-box project-work cat">
                    <span>Express Serenity</span>
                  </div>
                </div>

                <div class="title-text-header">
                  <div class="title-text-header-inner">
                    <a class="" >
                      Green Architects
                    </a>
                  </div>
                </div>

              </div>
            </div>
            <div class="image-container">
              <div class="headefr-fexid headefr-fexid-onepage" data-dsn-header="project">
                <div class="bg has-top-bottom" id="dsn-hero-parallax-img">
                  <div class=" " data-dsn="video" data-overlay="4" data-dsn-ajax="img">
                    <video id="video-header" class="image-bg cover-bg dsn-video" poster="<?php echo base_url ('assets_frontend/img/home/green-architects-header.png')?>" autoplay loop playsinline muted>
                      <source src="<?php echo base_url ('assets_frontend/video/green-architects-video.mp4')?>" type="video/mp4">
                        <source src="<?php echo base_url ('assets_frontend/video/green-architects-video.webm')?>" type="video/webm">
                          Your browser does not support HTML5 video.
                        </video>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="slide-item swiper-slide">
                <div class="slide-content">
                  <div class="slide-content-inner">
                    <div class="project-metas">
                      <div class="project-meta-box project-work cat">
                        <span>Green Architects</span>
                      </div>
                    </div>

                    <div class="title-text-header">
                      <div class="title-text-header-inner">
                        <a class="" >
                          Office Lobby
                        </a>
                      </div>
                    </div>

                    <div class="link-custom">
                      <a href="<?php echo site_url ('en-us/office')?>" class="image-zoom " data-dsn="parallax">
                        <span>Visit Our Office</span>
                      </a>
                    </div>

                  </div>
                </div>
                <div class="image-container">
                  <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>"
                    data-overlay="0">
                    <img src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>" alt="">
                  </div>
                </div>
              </div>

              <div class="slide-item swiper-slide">
                <div class="slide-content">
                  <div class="slide-content-inner">
                    <div class="project-metas">
                      <div class="project-meta-box project-work cat">
                        <span>Green Architects</span>
                      </div>
                    </div>

                    <div class="title-text-header">
                      <div class="title-text-header-inner">
                        <a class="" >
                          Meeting Room
                        </a>
                      </div>
                    </div>

                    <div class="link-custom">
                      <a href="<?php echo site_url ('en-us/office')?>" class="image-zoom " data-dsn="parallax"
                        >
                        <span>Visit Our Office</span>
                      </a>
                    </div>

                  </div>
                </div>
                <div class="image-container">
                  <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header2.jpg')?>"
                    data-overlay="0">
                    <img src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>" alt="">
                  </div>
                </div>
              </div>

              <div class="slide-item swiper-slide">
                <div class="slide-content">
                  <div class="slide-content-inner">
                    <div class="project-metas">
                      <div class="project-meta-box project-work cat">
                        <span>Green Architects</span>
                      </div>
                    </div>

                    <div class="title-text-header">
                      <div class="title-text-header-inner">
                        <a class="" >
                          Auditorium
                        </a>
                      </div>
                    </div>

                    <div class="link-custom">
                      <a href="<?php echo site_url ('en-us/office')?>" class="image-zoom " data-dsn="parallax"
                        >
                        <span>Visit Our Office</span>
                      </a>
                    </div>

                  </div>
                </div>
                <div class="image-container">
                  <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header3.jpg')?>"
                    data-overlay="0">
                    <img src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>" alt="">
                  </div>
                </div>
              </div>

              <div class="slide-item swiper-slide">
                <div class="slide-content">
                  <div class="slide-content-inner">
                    <div class="project-metas">
                      <div class="project-meta-box project-work cat">
                        <span>Green Architects</span>
                      </div>
                    </div>

                    <div class="title-text-header">
                      <div class="title-text-header-inner">
                        <a class="" >
                          Office Lounge
                        </a>
                      </div>
                    </div>


                    <div class="link-custom">
                      <a href="<?php echo site_url ('en-us/office')?>" class="image-zoom " data-dsn="parallax"
                        >
                        <span>Visit Our Office</span>
                      </a>
                    </div>

                  </div>
                </div>
                <div class="image-container">
                  <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header4.jpg')?>"
                    data-overlay="0">
                    <img src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class="dsn-slider-content"></div>


        <div class="nav-slider">
          <div class="swiper-wrapper" role="navigation">
            <div class="swiper-slide">
              <div class="image-container">
                <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/green-architects-header.png')?>"
                  data-overlay="2">
                </div>
              </div>
              <div class="content">
                <p>01</p>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="image-container">
                <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>"
                  data-overlay="2">
                </div>
              </div>
              <div class="content">
                <p>02</p>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="image-container">
                <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header2.jpg')?>"
                  data-overlay="2">
                </div>
              </div>
              <div class="content">
                <p>03</p>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="image-container">
                <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header3.jpg')?>"
                  data-overlay="2">
                </div>
              </div>
              <div class="content">
                <p>04</p>
              </div>
            </div>

            <div class="swiper-slide">
              <div class="image-container">
                <div class="image-bg cover-bg" data-image-src="<?php echo base_url ('assets_frontend/img/home/header4.jpg')?>"
                  data-overlay="2">
                </div>
              </div>
              <div class="content">
                <p>05</p>
              </div>
            </div>



          </div>
        </div>

        <section class="footer-slid" id="descover-holder">
          <div class="main-social">
            <div class="social-icon">
              <div class="social-btn">
                <div class="svg-wrapper">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.3 23.2">
                    <path
                    d="M19.4 15.5c-1.2 0-2.4.6-3.1 1.7L7.8 12v-.7l8.5-5.1c.7 1 1.9 1.6 3.1 1.6 2.1 0 3.9-1.7 3.9-3.9S21.6 0 19.4 0s-3.9 1.7-3.9 3.9v.4L7 9.3c-1.3-1.7-3.7-2-5.4-.8s-2.1 3.7-.8 5.4c.7 1 1.9 1.6 3.1 1.6s2.4-.6 3.1-1.6l8.5 5v.4c0 2.1 1.7 3.9 3.9 3.9s3.9-1.7 3.9-3.9c0-2.1-1.7-3.8-3.9-3.8zm0-13.6c1.1 0 1.9.9 1.9 1.9s-.9 1.9-1.9 1.9-1.9-.7-1.9-1.8.8-2 1.9-2zM3.9 13.6c-1.1 0-1.9-.9-1.9-1.9s.9-1.9 1.9-1.9 1.9.9 1.9 1.9-.8 1.9-1.9 1.9zm15.5 7.8c-1.1 0-1.9-.9-1.9-1.9s.9-1.9 1.9-1.9 1.9.9 1.9 1.9-.8 1.8-1.9 1.9z">
                  </path>
                </svg>
              </div>
            </div>
          </div>
          <ul class="social-network">
            <li>
              <a href="#" style="color:#fff">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li>
              <a href="#" style="color:#fff">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="#" style="color:#fff">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
            <li>
              <a href="#" style="color:#fff">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>

        <div class="control-num">
          <span class="sup active">01</span>
        </div>
        <div class="control-nav">
          <div class="prev-container" data-dsn="parallax">
            <svg viewBox="0 0 40 40">
              <path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
              <polyline class="path" points="14.6 17.45 20 22.85 25.4 17.45"></polyline>
            </svg>
          </div>

          <div class="next-container" data-dsn="parallax">
            <svg viewBox="0 0 40 40">
              <path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
              <polyline class="path" points="14.6 17.45 20 22.85 25.4 17.45"></polyline>
            </svg>
          </div>
        </div>

      </section>

    </div>
