<section class="contact-up section-margin section-padding">
    <div class="container">
        <div class="c-wapp">
            <a href="<?php echo site_url('en-us/careers')?>">
                <span class="hiring">
                    We are hiring
                </span>
                <span class="career">
                    Dare and contact us immediately!
                </span>
            </a>
        </div>
    </div>
</section>
