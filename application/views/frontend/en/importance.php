
<div class="container header-hero" style="margin-top: 60px;">
  <div class="row">
    <div class="col-lg-8">
      <div class="contenet-hero">
        <h5>MEANINGS OF</h5>
        <h2 style="z-index:1">The Importance Of Green Design</h2>
      </div>
    </div>
  </div>
</div>

<div class="wrapper">
  <div class="root-blog">
    <div class="container">

      <article class="post-list-item">
        <figure>
          <a class="image-zoom" data-dsn-animate="up">
            <img src="<?php echo base_url ('assets_frontend/img/home/importance_greendesign.jpg')?>" alt="">
          </a>
        </figure>
        <div class="post-list-item-content">
          <p>It is our duty as designers to guide people to create an energy-efficient living. According to the United Nations (2017), buildings account for nearly half of the world’s energy expenditures, 40% of greenhouse gas emissions, 25% of the earth’s potable water, and over 20% of all solid waste generated. Imagine if one high-rise building can save up to 60% of its energy, multiply it by a thousand building, how much money can we save to instead use it to build humanity.
            <br>
            <br>
            We are the carer of the world.
          </p><br>
          <h5>“We have sent you as a mercy for the whole world”. <i>Al-Anbiya (107)</i></h5>
        </div>
      </article>
    </div>
  </div>
  </div>
