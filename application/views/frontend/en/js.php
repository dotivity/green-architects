<!-- Optional JavaScript -->
<script src="<?php echo base_url ('assets_frontend/js/jquery-3.1.1.min.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/plugins.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/modernizr.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/dsn-grid.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/custom.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/skrollr.min.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/smooth-scrollbar.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/startf250.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/particle.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/gsap/TweenMax.min.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/gsap/ScrollMagic.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/gsap/animation.gsap.js')?>"></script>
<script src="<?php echo base_url ('assets_frontend/js/animation.js')?>"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2H49179QuTkfPvnbb7BpKpqA40IcDCpI&map_ids=7688f6a0d0448571&callback=initMap">
</script>
<script type="text/javascript">
map = new google.maps.Map(document.getElementById('map'), {
  center: {lat: -6.290354539788058, lng: 106.80960655532841},
  zoom: 8,
  mapId: '7688f6a0d0448571'
});
</script>


<!--skroll effect-->
<script type="text/javascript">
if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(window).width() > 767) { // only init skrollr on non-mobile devices
  skrollr.init();
}

$(window).on('resize', function () {
  if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) { // no reason to destroy on mobile
    if ($(window).width() <= 767) {
      skrollr.init().destroy(); // skrollr.init() returns the singleton created above
    }
  }

});
</script>

<script type="text/javascript">
$(document).ready(function(){
  var mouseX, mouseY;
  var ww = $( window ).width();
  var wh = $( window ).height();
  var traX, traY;
  $(document).mousemove(function(e){
    mouseX = e.pageX;
    mouseY = e.pageY;
    traX = ((4 * mouseX) / 570) + 40;
    traY = ((4 * mouseY) / 570) + 50;
    console.log(traX);
    $(".letter-effect").css({"background-position": traX + "%" + traY + "%"});
    $(".letter-effect-2").css({"background-position": traX + "%" + traY + "%"});
  });
});

</script>

<script>
var calcHeight = function() {
  $('#preview-frame-corporatevalue').height($(window).height());
}

$(document).ready(function() {
  calcHeight();
});

$(window).resize(function() {
  calcHeight();
}).load(function() {
  calcHeight();
});
</script>

<script>
var calcHeight = function() {
  $('#preview-frame1').height($(window).height());
}

$(document).ready(function() {
  calcHeight();
});

$(window).resize(function() {
  calcHeight();
}).load(function() {
  calcHeight();
});
</script>


<script>
    document.getElementById('video-header').play();
</script>
