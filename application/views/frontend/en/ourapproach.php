<div class=" box-gallery-vertical section-margin" data-dsn="color">
  <div class="mask-bg-droow"></div>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 ">
        <div class="box-im" data-dsn-grid="move-up">
          <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/our_approach.png')?>" alt=""
          data-dsn-move="20%">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-info">
          <div class="vertical-title" data-dsn-animate="up">
            <h2>Our Approach</h2>
          </div>
          <h6 data-dsn-animate="up">We celebrate local materials, embrace the culture, and support local artists. It is an investment to be environmentally friendly. </h6>
          <p data-dsn-animate="up">We will guide you to save energy and cost. We also provide solutions so that everyone can contribute to lower carbon by using low/zero-carbon technologies.</p>
        </div>
      </div>
    </div>
  </div>
</div>
