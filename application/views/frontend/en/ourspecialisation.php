<div class="box-gallery-vertical section-margin section-padding" >
  <div class="mask-bg-droow"></div>
  <div class="container">
    <div class="row align-items-center">

      <div class="col-lg-6">
        <div class="box-info">
          <div class="vertical-title" data-dsn-animate="up">
            <h2>Our Priority</h2>
          </div>
          <h6 data-dsn-animate="up">Our top priority is on the Zero/low carbon buildings approach. </h6>
          <p data-dsn-animate="up">However, in our home, defining sustainable and modern design in Indonesia that reflect or provide for our culture is also as important. Mixing elements to create something fresh and customised to the character of the people and place.</p>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-im" data-dsn-grid="move-up">
          <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/our_specialisation.png')?>" alt=""
          data-dsn-move="20%">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="one-title">
    <div class="title-sub-container">
      <p class="title-sub">THIS IS</p>
    </div>
    <h2 class="title-main">Our Specialisations</h2>
  </div>
</div>
<div class="embed-responsive1">
  <iframe id="preview-frame1" class="embed-responsive-item1" src="https://corporatevalue.bygreenarchitects.com/our-specialisations/" ></iframe>
</div>
