<?php
include __DIR__ .('/../../html.php');
include __DIR__ .('/../../openbody.php');
include __DIR__ .('/../../preloader.php');
include __DIR__ .('/../../nav.php');
?>

<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="headefr-fexid" data-dsn-header="project">
        <div class="bg" id="dsn-hero-parallax-img" data-dsn-ajax="img">
          <div class="bg-image  cover-bg" data-overlay="2"
          data-image-src="<?php echo base_url ('assets_frontend/img/charge/02.header/1.jpg')?>"></div>
        </div>

        <div class="scroll" data-dsn-animate="ajax">
          <span class="background"></span>
          <span class="triangle"></span>
        </div>
        <div class="project-title" id="dsn-hero-parallax-title">
          <div class="title-text-header">
            <div class="cat">
                <span>Sustainable Home</span>
            </div>
            <span class="title-text-header-inner">
              <span data-dsn-animate="ajax">In Harmony</span>
            </span>
          </div>

          <div class="sub-text-header" data-dsn-animate="ajax">
            <h5>Finished Project</h5>
            <span>- at South Jakarta</span>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-project">
        <div class="container intro-project section-margin">
          <div class="intro-text">
            <div class="title-cover" data-dsn-grid="move-section" data-dsn-opacity="0.1"
            data-dsn-duration="170%" data-dsn-move="0%">
            In Harmony
          </div>
          <div class="inner">
            <h2 data-dsn-animate="text">The Importance of Green Design</h2>
            <p data-dsn-animate="up">It is our duty as designers to guide people to create an energy-efficient living. According to the United Nations (2017), buildings account for nearly half of the world’s energy expenditures, 40% of greenhouse gas emissions, 25% of the earth’s potable water, and over 20% of all solid waste generated. Imagine if one high-rise building can save up to 60% of its energy, multiply it by a thousand building, how much money can we save to instead use it to build humanity.</p>
            <ul class="mt-20" data-dsn-animate="up">
              <li>Architect : Rima Nurani</li>
              <li>Project : Sustainable Home</li>
              <li>Status : Finished Project</li>
            </ul>

          </div>
        </div>
      </div>

      <div class="gallery-portfolio section-margin">
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/10.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/11.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/12.jpg')?>" alt="">
        </a>

      </div>

      <div class=" box-gallery-vertical section-margin section-padding">
        <div class="mask-bg"></div>
        <div class="container">
          <div class="row align-items-center h-100">
            <div class="col-lg-6 ">
              <div class="box-im" data-dsn-grid="move-up">
                <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/charge/04.section2/1.jpg')?>" alt=""
                data-dsn-move="20%">
              </div>
            </div>

            <div class="col-lg-6">


              <div class="box-info">

                <div class="vertical-title">
                  <h2 data-dsn-animate="up">GREETINGS</h2>
                </div>

                <h6 data-dsn-animate="up">From Rima Nurani</h6>

                <p data-dsn-animate="up">“After graduating from Australia, I see there is a need to focus on sustainable design, I have seen how architecture can disconnect or connect people. I believe we are the carer of the world, and we should dream…” </p>

              </div>
            </div>

          </div>
        </div>
      </div>





      <!--section 1-->

<div class="container-fluid gallery-col section-margin">
  <div class="row">
    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/1.jpg')?>" alt="">
        </a>

        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/2.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/3.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/4.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

  </div>
</div>

      <!--section 2-->

<div class="container-fluid gallery-col section-margin">
  <div class="row">
    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/1.jpg')?>" alt="">
        </a>

        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/2.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/3.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/4.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

  </div>
</div>

<!--section 3
<div class="container-fluid gallery-col section-margin">
  <div class="row">
    <div class="col-md-4 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/1.jpg')?>" alt="">
        </a>

        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-4 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/2.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-4 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/3/3.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

  </div>
</div>-->

    </div>

  </div>
</div>
<?php
include __DIR__ .('/../../footer.php');
include __DIR__ .('/../../waitloader.php');
include __DIR__ .('/../../cursor.php');
include __DIR__ .('/../../js.php');
include __DIR__ .('/../../closebody.php');
include __DIR__ .('/../../end.php');
?>
