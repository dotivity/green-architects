

<div class="box-seat box-seat-full section-margin">
    <div class="container-fluid">
        <div class="inner-img" data-dsn-grid="move-up">
            <img data-dsn-scale="1" data-dsn-y="30%" src="<?php echo base_url ('assets_frontend/img/charge/01.section/1.jpg')?>" alt="">
        </div>
        <div class="pro-text">
            <h3> CHARGE</h3>
            <p>As people spend more time indoors, and even more after the corona virus outbreak which obliged people to stare at computer screens, a well-designed spaces are extremely important. Thus, we broke down walls and open up the space and now the east-facing living area bathed in morning light. Natural sunlight can elevate mood which can re-energise people throughout the day.</p>
            <div class="link-custom">
                <a class="image-zoom effect-ajax" href="<?php echo site_url ('en-us/charge')?>" data-dsn="parallax">
                    <span>See More</span>
                </a>
            </div>
        </div>
    </div>
</div>
