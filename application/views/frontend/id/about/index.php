<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>
<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="header-hero header-hero-2 ">
        <div class="container h-100">
          <div class="row align-items-center h-100">
            <div class="col-lg-12">
              <div class="contenet-hero">
                <h1>Express Serenity</h1>
                <p>Studio berbasis di Indonesia untuk arsitektur & desain interior. Mengubah sektor konstruksi menjadi bangunan yang sustainable.</p>
              </div>
            </div>
          </div>
        </div>
      </header>

      <section class="intro-about section-margin">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="intro-content-text">

                <h2 data-bottom="opacity: 0;margin-right:150px" data-center="opacity:1;margin-right:0px">
                  Hi, <br /> kami adalah Green Architects
                </h2>

                <p data-dsn-animate="text" data-bottom="opacity: 0;" data-center="opacity:1;">
                  Kami adalah konsultan desain terkemuka dan paling berpengaruh yang berkomitmen untuk menciptakan bangunan <strong>rendah</strong> <strong>karbon</strong> yang memiliki dampak sosial dan bermakna, serta memberi perubahan yang berarti terhadap perkotaan.<br>
                </p>
                <p data-dsn-animate="text" data-bottom="opacity: 0;" data-center="opacity:1;">
                  Indonesia merupakan negara yang rentan terhadap <strong>perubahan</strong> <strong>iklim</strong> oleh karena itu kami hadir untuk meningkatkan kualitas hidup, menghemat tagihan listrik, dan meningkatkan nilai properti yang anda miliki.<br>
                </p>
                <p data-dsn-animate="text" data-bottom="opacity: 0;" data-center="opacity:1;">
                  Kami juga merupakan konsultan desain <strong>interdisipliner</strong>  dengan kumpulan arsitek, desainer interior, desainer lanskap, peneliti, desainer perkotaan dan kontraktor.
                </p>

                <h6 data-dsn-animate="text"  data-bottom="opacity: 0;margin-left:240px" data-center="opacity:1;margin-left:0px">Liverpool, Northwest England</h6>

                <div class="exper" data-bottom="opacity: 0;margin-left:200px" data-center-top="opacity:1;margin-left:0px">
                  <div class="numb-ex">
                    <span class="word" data-dsn-animate="text">2018</span>
                  </div>

                  <h4 data-dsn-animate="up">GREEN ARCHITECTS <br> DIDIRIKAN</h4>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="background-mask">
          <div class="background-mask-bg-droow"></div>
          <div class="img-box">
            <div class="img-cent" data-dsn-grid="move-up">
              <div class="svg-wrapper-custom" >
                <svg height="100%" width="100%" xmlns="http://www.w3.org/2000/svg">
                  <rect id="route-1" class="shape-custom" height="100%" width="100%" />
                </svg>
              </div>
              <div  id="trigger-route-1" class="img-container">
                <img src="<?php echo base_url ('assets_frontend/img/home/about_us1.jpg')?>" alt="">
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="wrapper">
        <div class="root-about">
          <div class="box-seat box-seat-full">
            <div class="container-fluid">
              <div class="letter-effect" data-bottom="opacity: 0;margin-right:500px" data-200-center="opacity:1;margin-right:200px">Green</div>
              <div class="inner-img" data-dsn-grid="move-up">
                <img src="<?php echo base_url ('assets_frontend/img/home/about_us2.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
              </div>
              <div class="pro-text">
                <p data-bottom="opacity: 0" data-center="opacity:1">
                  Untuk memberikan layanan dan hasil terbaik, kami menggunakan solusi desain dengan <b>Western standard</b>  dan teknologi terkini. Selain itu, pada era digital ini, kami berinvestasi pada nilai-nilai dan visi perusahaan kami untuk menjadi semakin terdepan dan handal dalam perubahan.<br><br>
                  Kami memiliki ahli dari berbagai penjuru dunia seperti Inggris, India, Meksiko, dan Arab Saudi yang berkomitmen untuk mempromosikan desain yang sustainable. Dengan latar belakang <b>ahli yang beragam</b>, kami yakin dapat memberikan solusi terbaik untuk masa depan yang lebih tangguh.
                </p>
              </div>
            </div>
          </div>


          <div class="box-seat-v1 box-seat-full">
            <div class="container-fluid">
              <div class="pro-text" style="z-index:10">
                <p  data-bottom="opacity: 0;" data-center="opacity:1;">
                  Berfokus pada desain sustainable adalah hal yang terpenting bagi kami. Dengan memilih untuk merancang dan membangun bersama kami, maka anda berkontribusi dalam mempromosikan <b>lingkungan kerja yang aman</b>  dan membantu kami untuk mengatasi salah satu masalah terbesar yang dihadapi oleh bangsa Indonesia, yaitu pekerja konstruksi yang tidak bersertifikat.<br><br>
                  Inilah yang dimaksud dengan branding kami, untuk menggandeng tangan setiap masyarakat dan mitra kami untuk menciptakan dunia yang lebih baik.
                </p>
              </div>
              <div class="letter-effect-2" data-bottom="opacity: 0;margin-left:500px" data-200-center="opacity:1;margin-left:200px">Architects</div>
              <div class="inner-img-v1" data-dsn-grid="move-up">
                <img src="<?php echo base_url ('assets_frontend/img/home/about_us3.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
              </div>
            </div>
          </div>
        </div>
      </div>


          <?php
          include __DIR__ .('/../footer.php');
          include __DIR__ .('/../waitloader.php');
          include __DIR__ .('/../cursor.php');
          include __DIR__ .('/../js.php');
          include __DIR__ .('/../closebody.php');
          include __DIR__ .('/../end.php');
          ?>
