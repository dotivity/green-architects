<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root">
  <div id='my-scrollbar' data-scrollbar>
    <header>
      <div class="headefr-fexid" data-dsn-header="project">
        <div class="bg" data-dsn-ajax="img">
          <div class="bg-image cover-bg" data-overlay="3"
          data-image-src="<?php echo base_url ('assets_frontend/img/greetings/ari.jpg')?>"></div>
        </div>

        <div class="scroll" data-dsn-animate="ajax">
          <span class="background"></span>
          <span class="triangle"></span>
        </div>
        <div class="project-title" id="" style="top:120px">
          <div class="title-text-header">
            <div class="cat">
              <span>President Commissioner | Founder ESQ Group</span>
            </div>
            <span class="title-text-header-inner">
              <span data-dsn-animate="ajax">Dr. (H.C) Ary Ginanjar Agustian</span>
            </span>
          </div>

          <div class="sub-text-header" data-dsn-animate="ajax">
            <h5>Dipublikasi</h5>
            <span>- 11 November 2020</span>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-project">
        <div class="container intro-project section-margin">
          <div class="intro-text">
            <div class="inner">
              <h2 class="title-main" style="margin-bottom:50px">Sambutan Dari Komisaris Utama</h2>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                “Saat ini kita banyak sekali bicara tentang energi terbaharukan, tapi kita lupa energi yang terlupakan. Orang sibuk bagaimana membangun energi baru,<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                tapi orang lupa bagaimana kita menghemat dan menata energi dari sisi yang berbeda, yaitu dari sisi Arsitek. Tidak terhitung biaya-biaya yang <br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                dikeluarkan akibat gedung-gedung yang tidak memiliki dasar-dasar sustainable arsitektur sehingga tidak terhitung kerugian yang ditimbulkan, <br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                biaya-biaya yang harus dimunculkan akibat dari tidak efektif dan tidak efisiennya. Kita bisa bayangkan apabila satu gedung bisa menghemat sekitar <br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                Rp. 100.000.000 saja dalam sebulan, dan dikalikan seribu atau bahkan sepuluh ribu gedung, berapa uang yang bisa didapatkan dari hasil hitungan<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                tersebut? Dimana uang tersebut tentu bisa dipergunakan untuk membangun kemanusiaan, disaat bangsa Indonesia membutuhkan begitu banyak<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                pertolongan ditengah suasana yang penuh dengan tantangan secara ekonomi ini”.
              </p>
              <br><br>

              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                “Green Architects bukan hanya menciptakan sebuah ekosistem yang sustainable, tetapi bagaimana kita bisa meningkatkan kualitas kehidupan. Kehadiran<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                Green Architects ini menjadi penting di Indonesia dalam rangka menuju Indonesia Emas 2045 yaitu Indonesia adidaya, Adidaya itu akan sulit tercapai<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                kalau kita boros energi, gedung-gedung kita masih belum menggunakan green architecture, jauh dari green building yang mengakibatkan terbuangnnya<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                energi-energi yang sesungguhnya harus bisa dimanfaatkan untuk kebutuhan yang lain, untuk membangun peradaban di Indonesia. Selamat atas<br>
              </p>
              <p class="greetings-desktop" data-bottom="opacity: 0;transform: scale(0.5);" data-100-center="opacity:1;transform: scale(1);" style="white-space: nowrap; overflow: hidden; font-size:14px">
                kehadiran Green Architects, semoga kontribusi nyatanya akan membantu percepatan menuju Indonesia Emas 2045. Salam Green Architects!”.
              </p>

              <p class="greetings-mobile" style="margin-top: -50px;">
                “Saat ini kita banyak sekali bicara tentang energi terbaharukan, tapi kita lupa energi yang terlupakan. Orang sibuk bagaimana membangun energi baru, tapi orang lupa bagaimana kita menghemat dan menata energi dari sisi yang berbeda, yaitu dari sisi Arsitek. Tidak terhitung biaya-biaya yang dikeluarkan akibat gedung-gedung yang tidak memiliki dasar-dasar sustainable arsitektur sehingga tidak terhitung kerugian yang ditimbulkan, biaya-biaya yang harus dimunculkan akibat dari tidak efektif dan tidak efisiennya. Kita bisa bayangkan apabila satu gedung bisa menghemat sekitar Rp. 100.000.000 saja dalam sebulan, dan dikalikan seribu atau bahkan sepuluh ribu gedung, berapa uang yang bisa didapatkan dari hasil hitungan tersebut? Dimana uang tersebut tentu bisa dipergunakan untuk membangun kemanusiaan, disaat bangsa Indonesia membutuhkan begitu banyak pertolongan ditengah suasana yang penuh dengan tantangan secara ekonomi ini”.<br><br>

                “Green Architects bukan hanya menciptakan sebuah ekosistem yang sustainable, tetapi bagaimana kita bisa meningkatkan kualitas kehidupan. Kehadiran Green Architects ini menjadi penting di Indonesia dalam rangka menuju Indonesia Emas 2045 yaitu Indonesia adidaya, Adidaya itu akan sulit tercapai kalau kita boros energi, gedung-gedung kita masih belum menggunakan green architecture, jauh dari green building yang mengakibatkan terbuangnnya energi-energi yang sesungguhnya harus bisa dimanfaatkan untuk kebutuhan yang lain, untuk membangun peradaban di Indonesia. Selamat atas kehadiran Green Architects, semoga kontribusi nyatanya akan membantu percepatan menuju Indonesia Emas 2045. Salam Green Architects!”.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="wrapper">
      <div class="root-about">

        <div class="box-seat box-seat-full">
          <div class="container-fluid">
            <div class="inner-img d-none d-sm-block" data-dsn-grid="move-up">
              <img class="reza-photos" src="<?php echo base_url ('assets_frontend/img/greetings/rima.jpg')?>" alt="" data-dsn-y="35%" data-dsn-scale="1">
            </div>
            <div class="inner-img d-block d-sm-none">
              <img class="reza-photos" style="object-fit: cover; object-position: 10% 50%;" src="<?php echo base_url ('assets_frontend/img/greetings/rima.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
            </div>
            <div class="pro-text">
              <h3 style="padding-bottom: 40px;"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">SAMBUTAN DEWAN DIREKSI </h3>
              <h3 style="font-size:1.6em"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Rima Khansa Nurani, B.A.Sc., M.Sc.</h3>
              <h3 style="font-size:1em"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Managing Director | University of Liverpool graduate</h3>
              <p style="font-size:13px" data-dsn-animate="up"  data-bottom-center="opacity: 0;" data-200-center="opacity:1;">
                “Setelah menyelesaikan pendidikan dari Australia, saya melihat adanya kebutuhan desain bangunan yang sustainable. Saya telah melihat bagaimana arsitektur dapat memutuskan atau menghubungkan setiap orang. Saya percaya kita semua adalah penjaga bumi, dan kita harus bermimpi serta memiliki tujuan yang lebih tinggi. Oleh karena itu saya memutuskan untuk melanjutkan pendidikan di Sustainable Environmental Design in Architecture (SEDA) di Inggris untuk menjelajahi konsep tersebut.<br><br>
                Selama melakukan proses penelitian dan mengunjungi berbagai negara, saya menemukan adanya ketimpangan antara gedung-gedung di Indonesia dan dunia. Oleh sebab itu, melalui Green Architects, kami berusaha untuk meningkatkan desain yang sustainable, yang memiliki nilai pembelajaran, serta mampu meningkatkan produktivitas dan kesejahteraan banyak orang”.
              </p>
            </div>
          </div>
        </div>

        <div class="box-seat box-seat-full">
          <div class="container-fluid">
            <div class="inner-img d-none d-sm-block" data-dsn-grid="move-up">
              <img class="reza-photos" src="<?php echo base_url ('assets_frontend/img/greetings/reza.jpg')?>" alt="" data-dsn-y="35%" data-dsn-scale="1">
            </div>
            <div class="inner-img d-block d-sm-none">
              <img class="reza-photos" style="object-fit: cover; object-position: 80% 50%;" src="<?php echo base_url ('assets_frontend/img/greetings/reza.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
            </div>
            <div class="pro-text1">
              <h3 style="padding-bottom: 40px;"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">SAMBUTAN DEWAN DIREKSI </h3>
              <h3 style="font-size:1.6em"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Ahmad Reza Hariyadi, S.M., M.Sc.</h3>
              <h3 style="font-size:1em"  data-bottom-center="opacity: 0;" data-bottom="opacity:1;">Executive Director | Liverpool John Moores University graduate</h3>
              <p style="font-size:13px" data-dsn-animate="up"  data-bottom-center="opacity: 0;" data-200-center="opacity:1;">“Tidak hanya fokus kepada aspek lingkungan dari desain yang kami hasilkan, Green Architects juga fokus kepada kesejahteraan stakeholders’ dan shareholders yang kami miliki. Sehingga proses bisnis yang kami jalankan bisa berkesinambungan dan terus memberikan dampak positif kepada lingkungan, masyarakat, dan negara. Selain itu, untuk memberikan pelayanan terbaik kepada konsumen, Green Architects juga terus berpacu dengan waktu untuk menerapkan konsep terbaru serta penggunaan teknologi terkini dalam implementasi desain yang kami hasilkan”.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <?php
    include __DIR__ .('/../footer.php');
    include __DIR__ .('/../waitloader.php');
    include __DIR__ .('/../cursor.php');
    include __DIR__ .('/../js.php');
    include __DIR__ .('/../closebody.php');
    include __DIR__ .('/../end.php');
    ?>
