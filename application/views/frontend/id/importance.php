
<div class="container header-hero" style="margin-top: 60px;">
  <div class="row">
    <div class="col-lg-8">
      <div class="contenet-hero">
        <h5>PENTINGNYA </h5>
        <h2 style="z-index:1">DESAIN YANG SUSTAINABLE </h2>
      </div>
    </div>
  </div>
</div>

<div class="wrapper">
  <div class="root-blog">
    <div class="container">

      <article class="post-list-item">
        <figure>
          <a class="image-zoom" data-dsn-animate="up">
            <img src="<?php echo base_url ('assets_frontend/img/home/importance_greendesign.jpg')?>" alt="">
          </a>
        </figure>
        <div class="post-list-item-content">
          <p>Merupakan tugas kita sebagai desainer untuk membimbing orang-orang untuk menciptakan kehidupan yang energi efisien. Menurut United Nations Environment Programme (2017), bangunan menyumbang hampir setengah dari pengeluaran energi dunia, 40% emisi gas rumah kaca, 25% dari air minum bumi, dan lebih dari 20% dari semua limbah padat yang dihasilkan. Bayangkan jika satu gedung bertingkat dapat menghemat hingga 60% energinya, dikalikan dengan 1000 bangunan, maka berapa banyak uang yang dapat kita hemat untuk digunakan dalam pembangunan kesejahteraan umat manusia.

            <br><br>Kita adalah penjaga bumi.

          </p><br>
          <h5>“Dan tiadalah Kami mengutus kamu, melainkan untuk (menjadi) rahmat bagi semesta alam”. <i>Al-Anbiya (107)</i></h5>
        </div>
      </article>
    </div>
  </div>
