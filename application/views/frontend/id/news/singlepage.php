<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS" />
  <meta name="description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design.">
  <meta name="keywords" content=" GREEN ARCHITECTS - Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS ">
  <meta name="author" content=" GREEN ARCHITECTS ">
  <meta name="identifier-URL" content="https://www.bygreenarchitects.com">
  <meta property="og:title" content="Jasa Arsitek Jakarta - Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design – GREEN ARCHITECTS"/>
  <meta property="og:url" content="https:// www.bygreenarchitects.com "/>
  <meta property="og:type" content="website" />
  <meta property="og:description" content="GREEN ARCHITECTS adalah perusahaan Konsultan arsitektur yang secara khusus berfokus pada bidang Arsitek, Interior Desain, Bangunan Komersial, Desain Sustainable, Rumah Ramah Lingkungan, Gedung Ramah Lingkungan, Desain landscape, Urban Design."/>
  <meta property="og:image" content="<?php echo base_url ('assets_frontend/meta/meta-home.jpg')?>">
  <meta property="og:site_name" content=" Jasa Arsitek Jakarta – GREEN ARCHITECTS"/>

  <!--  Title -->
  <title><?php echo $data->pic_title?></title>

  <!-- Font Google -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&amp;display=swap" rel="stylesheet">

  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />
  <link rel="icon" href="<?php echo base_url ('assets_frontend/img/ico/favicon.ico')?>" type="image/x-icon" />

  <!-- custom styles (optional) -->
  <link href="<?php echo base_url ('assets_frontend/css/plugins.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url ('assets_frontend/css/style.css')?>" rel="stylesheet" />
  <!--<link href="<?php echo base_url ('assets_frontend/css/bootstrap.min.css')?>" rel="stylesheet" />-->

  <!-- font awesome -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_frontend/assets_blur_effect/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_frontend/assets_blur_effect/fonts/font-awesome/css/font-awesome.css')?>">

  <!-- REVOLUTION STYLE SHEETS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets_frontend/assets_blur_effect/css/rs6.css')?>">

  <!-- REVOLUTION LAYERS STYLES -->
  <style type="text/css"></style>


  <!-- STYLE -->
  <style type="text/css">.blurslider-gradient{background:-webkit-linear-gradient(left,#FFE552 0%,#46724B 50%,#A7DA36 100%);-webkit-background-clip:text;-webkit-text-fill-color:transparent}.blurslider-gradient:after{text-shadow:2px 2px 2px #000000;  color:transparent}.blurslider-line{background:-webkit-linear-gradient(left,#FFE552 0%,#46724B 50%,#A7DA36 100%) !important}</style><!-- /STYLE --><!-- STYLE -->				<style type="text/css">	#rev_slider_23_1_wrapper rs-loader.spinner3 div { background-color: #333333 !important; } </style><!-- /STYLE --><!-- STYLE -->				<style type="text/css">#rev_slider_23_1_wrapper .uranus.tparrows{width:50px; height:50px; background:rgba(255,255,255,0)}#rev_slider_23_1_wrapper .uranus.tparrows:before{width:50px; height:50px; line-height:50px; font-size:40px; transition:all 0.3s;-webkit-transition:all 0.3s}#rev_slider_23_1_wrapper .uranus.tparrows:hover:before{opacity:0.75}</style><!-- /STYLE -->

  <script type="text/javascript">function setREVStartSize(e){
    //window.requestAnimationFrame(function() {
    window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;
    window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;
    try {
      var pw = document.getElementById(e.c).parentNode.offsetWidth,
      newh;
      pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
      e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
      e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
      e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
      e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
      e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
      e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
      e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);
      if(e.layout==="fullscreen" || e.l==="fullscreen")
      newh = Math.max(e.mh,window.RSIH);
      else{
        e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
        for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];
        e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
        e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
        for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];

        var nl = new Array(e.rl.length),
        ix = 0,
        sl;
        e.tabw = e.tabhide>=pw ? 0 : e.tabw;
        e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
        e.tabh = e.tabhide>=pw ? 0 : e.tabh;
        e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;
        for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
        sl = nl[0];
        for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}
        var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
        newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
      }
      if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
      document.getElementById(e.c).height = newh+"px";
      window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";
    } catch(e){
      console.log("Failure at Presize of Slider:" + e)
    }
    //});
  };</script>

</head>

<?php
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="header-single-post" data-dsn-header="project">
        <div class="container">

          <div class="inner-box m-section">
            <div class="post-info">
              <a href="#" class="blog-post-date dsn-link"><?php echo $data->pic_date_picker?></a>
            </div>

            <h3 class="title-box mt-10"><?php echo $data->pic_title1?>
            </h3>
          </div>

        </div>

      </div>
    </header>

    <div class="wrapper">
      <div class="container section-margin">
        <div class="news-content">
          <div class="news-content-inner">
            <div class="News-socials-wrapper">

              <div>

                <div class="post-share">
                  <h5 class="title-caption">Data Proyek: </h5>
                  <ul>
              <?php echo $data->pic_project1?>
                  </ul>
                </div>
              </div>

            </div>

            <div class="post-content">
              <?php echo $data->pic_desc1?>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</main>

<?php
include __DIR__ .('/../footer.php');
include __DIR__ .('/../waitloader.php');
include __DIR__ .('/../cursor.php');
include __DIR__ .('/../js.php');
include __DIR__ .('/../closebody.php');
include __DIR__ .('/../end.php');
?>
