<div class=" box-gallery-vertical section-margin" data-dsn="color">
  <div class="mask-bg-droow"></div>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 ">
        <div class="box-im" data-dsn-grid="move-up">
          <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/our_approach.png')?>" alt=""
          data-dsn-move="20%">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-info">
          <div class="vertical-title" data-dsn-animate="up">
            <h2>PENDEKATAN KAMI </h2>
          </div>
          <h6 data-dsn-animate="up">Kami meberdayakan material lokal, merangkul budaya, dan mendukung seniman lokal. Sustainable merupakan investasi. Kami akan memandu Anda untuk menghemat energi dan biaya.  </h6>
          <p data-dsn-animate="up">Kami juga memberikan solusi agar setiap orang dapat berkontribusi pada pengurangan karbon dengan menggunakan teknologi rendah / nol karbon.</p>
        </div>
      </div>
    </div>
  </div>
</div>
