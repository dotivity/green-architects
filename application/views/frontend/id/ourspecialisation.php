<div class="box-gallery-vertical section-margin section-padding">
  <div class="mask-bg-droow"></div>
  <div class="container">
    <div class="row align-items-center">


      <div class="col-lg-6">
        <div class="box-info">
          <div class="vertical-title" data-dsn-animate="up">
            <h2>PRIORITAS KAMI </h2>
          </div>
          <h6 data-dsn-animate="up">Prioritas utama kami adalah melalui pendekatan bangunan Nol / rendah karbon.</h6>
          <p data-dsn-animate="up">Namun, di tempat kita berasal, memberikan sentuhan desain yang sustainable serta modern di Indonesia, yang mampu mencerminkan atau memberikan gambaran budaya yang kita miliki juga sama pentingnya. Memadukan elemen untuk menciptakan sesuatu yang baru yang mampu menyesesuaikan karakter masyarakat dan lingkungan sekitar.</p>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-im" data-dsn-grid="move-up">
          <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/our_specialisation.png')?>" alt=""
          data-dsn-move="20%">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="one-title">
    <div class="title-sub-container">
      <p class="title-sub">INI ADALAH</p>
    </div>
    <h2 class="title-main">Spesialisasi Kami</h2>
  </div>
</div>
<div class="embed-responsive1 embed-responsive-21by9">
  <iframe id="preview-frame1" class="embed-responsive-item" src="https://corporatevalue.bygreenarchitects.com/our-specialisations/" ></iframe>
</div>
