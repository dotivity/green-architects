<?php
include __DIR__ .('/../../html.php');
include __DIR__ .('/../../openbody.php');
include __DIR__ .('/../../preloader.php');
include __DIR__ .('/../../nav.php');
?>

<main class="main-root">
  <div id="dsn-scrollbar">
    <header>
      <div class="headefr-fexid" data-dsn-header="project">
        <div class="bg" id="dsn-hero-parallax-img" data-dsn-ajax="img">
          <div class="bg-image  cover-bg" data-overlay="2"
          data-image-src="<?php echo base_url ('assets_frontend/img/charge/02.header/1.jpg')?>"></div>
        </div>

        <div class="scroll" data-dsn-animate="ajax">
          <span class="background"></span>
          <span class="triangle"></span>
        </div>
        <div class="project-title" id="dsn-hero-parallax-title">
          <div class="title-text-header">
            <div class="cat">
                <span>-	RUMAH SUSTAINABLE</span>
            </div>
            <span class="title-text-header-inner">
              <span data-dsn-animate="ajax">In Harmony</span>
            </span>
          </div>

          <div class="sub-text-header" data-dsn-animate="ajax">
            <h5>Proyek Rampung</h5>
            <span>- di Jakarta Selatan</span>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-project">
        <div class="container intro-project section-margin">
          <div class="intro-text">
            <div class="title-cover" data-dsn-grid="move-section" data-dsn-opacity="0.1"
            data-dsn-duration="170%" data-dsn-move="0%">
            In Harmony
          </div>
          <div class="inner">
            <h2 data-dsn-animate="text">Pentingnya Desain yang Sustainable</h2>
            <p data-dsn-animate="up">Merupakan tugas kita sebagai desainer untuk membimbing orang-orang untuk menciptakan kehidupan yang energi efisien. Menurut United Nations Environment Programme (2017), bangunan menyumbang hampir setengah dari pengeluaran energi dunia, 40% emisi gas rumah kaca, 25% dari air minum bumi, dan lebih dari 20% dari semua limbah padat yang dihasilkan. Bayangkan jika satu gedung bertingkat dapat menghemat hingga 60% energinya, dikalikan dengan 1000 bangunan, maka berapa banyak uang yang dapat kita hemat untuk digunakan dalam pembangunan kesejahteraan umat manusia.</p>
            <ul class="mt-20" data-dsn-animate="up">
              <li>Arsitek : Rima Nurani</li>
              <li>Project : Rumah Sustainable</li>
              <li>Status : Proyek Rampung</li>
            </ul>

          </div>
        </div>
      </div>

      <div class="gallery-portfolio section-margin">
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/1.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/2.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/3.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/4.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/5.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/6.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/7.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/8.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/10.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/11.jpg')?>" alt="">
        </a>
        <a class="link-pop" href="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>"
          data-source="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/9.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/05.thumbnail/12.jpg')?>" alt="">
        </a>

      </div>

      <div class=" box-gallery-vertical section-margin section-padding">
        <div class="mask-bg"></div>
        <div class="container">
          <div class="row align-items-center h-100">
            <div class="col-lg-6 ">
              <div class="box-im" data-dsn-grid="move-up">
                <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/charge/04.section2/1.jpg')?>" alt=""
                data-dsn-move="20%">
              </div>
            </div>

            <div class="col-lg-6">


              <div class="box-info">

                <div class="vertical-title">
                  <h2 data-dsn-animate="up">SAMBUTAN</h2>
                </div>

                <h6 data-dsn-animate="up">Dari Rima Nurani</h6>

                <p data-dsn-animate="up">“Setelah menyelesaikan pendidikan dari Australia, saya melihat adanya kebutuhan desain bangunan yang sustainable. Saya telah melihat bagaimana arsitektur dapat memutuskan atau menghubungkan setiap orang. Saya percaya kita semua adalah penjaga bumi, dan kita harus bermimpi serta memiliki tujuan yang lebih tinggi.”</p>

              </div>
            </div>

          </div>
        </div>
      </div>

      <!--section 1-->

<div class="container-fluid gallery-col section-margin">
  <div class="row">
    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/1.jpg')?>" alt="">
        </a>

        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/2.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/3.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/1/4.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

  </div>
</div>

      <!--section 2-->

<div class="container-fluid gallery-col section-margin">
  <div class="row">
    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/1.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/1.jpg')?>" alt="">
        </a>

        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/2.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/2.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/3.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/3.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

    <div class="col-md-3 box-im section-padding">
      <div class="image-zoom" data-dsn="parallax">
        <a class="single-image" href="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/4.jpg')?>">
          <img src="<?php echo base_url ('assets_frontend/img/charge/03.section1/2/4.jpg')?>" alt="">
        </a>
        <div class="caption"></div>
      </div>
    </div>

  </div>
</div>




    </div>

  </div>
</div>


<?php
include __DIR__ .('/../../footer.php');
include __DIR__ .('/../../waitloader.php');
include __DIR__ .('/../../cursor.php');
include __DIR__ .('/../../js.php');
include __DIR__ .('/../../closebody.php');
include __DIR__ .('/../../end.php');
?>
