<section class="our-team our-team-not-bg section-margin section-padding">
    <div class="container">
        <div class="one-title" style="margin-bottom:40px">
            <div class="title-sub-container">
                <p class="title-sub">Tim Kami</p>
            </div>
            <h2 class="title-main">Kemitraan</h2>
        </div>

        <div class="post-list-item-content" style="margin-bottom:60px">
          <p>Di dunia global ini, kami menghargai interaksi antar budaya. Oleh karena itu setiap setengah tahun kami berbagi pandangan global untuk memberikan solusi. Kami membuka desain kami untuk ditinjau oleh mitra kami untuk memancing inovasi dalam menciptakan gaya dan desain yang unik.
          </p>
        </div>

        <div class="custom-container">
            <div class="slick-slider">
                <div class="team-item ">
                    <div class="box-img">
                        <img src="<?php echo base_url ('assets_frontend/img/team/berry.jpg')?>" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Berry Natalegawa</h4>
                        <p>United Kingdom</p>
                    </div>
                </div>

                <div class="team-item ">
                    <div class="box-img">
                        <img src="<?php echo base_url ('assets_frontend/img/team/saima.jpg')?>" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Saima Mirza</h4>
                        <p>India</p>
                    </div>
                </div>

                <div class="team-item ">
                    <div class="box-img">
                        <img src="<?php echo base_url ('assets_frontend/img/team/carlos.jpg')?>" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Carlos Aguirre</h4>
                        <p>Mexico</p>
                    </div>
                </div>

                <div class="team-item ">
                    <div class="box-img">
                        <img src="<?php echo base_url ('assets_frontend/img/team/alharthi.jpg')?>" alt="">
                    </div>

                    <div class="box-content">
                        <h4>Mohammed Alharthi</h4>
                        <p>Saudi Arabia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
