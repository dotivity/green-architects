<section class="our-work work-under-header  section-margin" data-dsn-col="3">

  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 offset-lg-3">
        <div class="work-container">
          <div class="slick-slider">
            <?php foreach ($portfolio as $data): ?>
            <div class="work-item slick-slide">
              <img class="has-top-bottom" src="<?=base_url()?>upload/portfolio/<?=$data->pic_file?>" alt="">
              <div class="item-border"></div>
              <div class="item-info">
                <a href="#" data-dsn-grid="move-up" class="effect-ajax">

                  <h5 class="cat"></h5>
                  <h4><?php echo $data->pic_title?></h4>
                  <span><span><?php echo $data->pic_desc1?></span></span>
                </a>

              </div>
            </div>
          <?php endforeach ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
