

<div class="box-seat box-seat-full section-margin">
    <div class="container-fluid">
        <div class="inner-img" data-dsn-grid="move-up">
            <img data-dsn-scale="1" data-dsn-y="30%" src="<?php echo base_url ('assets_frontend/img/charge/01.section/1.jpg')?>" alt="">
        </div>
        <div class="pro-text">
            <h3> CHARGE</h3>
            <p>Ketika orang menghabiskan lebih banyak waktu di dalam ruangan, dan terlebih lagi setelah wabah virus corona yang mengharuskan orang untuk sering menatap layar komputer, ruang yang dirancang dengan baik menjadi sangat penting. Jadi, kami merobohkan untuk membuka ruang dan sekarang ruang tamu yang menghadap ke timur bermandikan cahaya pagi. Sinar matahari alami dapat meningkatkan suasana hati yang dapat mengembalikan energi orang sepanjang hari.</p>
            <div class="link-custom">
                <a class="image-zoom effect-ajax" href="<?php echo site_url ('in-id/charge')?>" data-dsn="parallax">
                    <span>Lihat Selengkapnya</span>
                </a>
            </div>
        </div>
    </div>
</div>
