<section class="our-work work-under-header  section-margin" data-dsn-col="3">

<div class="container">
  <div class="one-title">
    <div class="title-sub-container">
      <p class="title-sub">Hasil Kami</p>
    </div>
    <h2 class="title-main">Desain Portfolio Kreatif</h2>
  </div>
</div>


<div class="box-seat">
  <div class="dsn-v-text">
    <div class="container-fluid">
      <div class="box-middle-text">
        <a id="play-video" class="video-play-button vid" href="https://youtu.be/2z4Fmqm3GEs"  data-toggle="modal">
          <span></span>
        </a>
      </div>
      <div class="inner-img" data-dsn-grid="move-up" data-overlay="9">
        <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/Thumbnail-Youtube.jpg')?>" alt="">
      </div>
    </div>
  </div>
</div>
<section>
