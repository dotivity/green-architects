<section class="client-see section-margin">
    <div class="container">
        <div class="inner">
            <div class="left">
                <h2 class="title" data-dsn-grid="move-section" data-dsn-move="-60"
                    data-dsn-duration="100%" data-dsn-opacity="1" data-dsn-responsive="tablet">
                    <span class="text">Visi Kami </span>
                </h2>
            </div>

            <div class="items">
                <div class="bg" data-bottom="opacity: 0;margin-left:240px" data-50-center="opacity:1;margin-left:0px"></div>
                    <div class="item">
                        <div class="quote">
                            <p data-bottom="opacity: 0" data-center="opacity:1">Membangun Indonesia yang sustainable dengan sudut pandang global, serta menjadi perusahaan arsitek terdepan di Asia Tenggara.</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
