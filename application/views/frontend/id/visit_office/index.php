<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root">
    <div id="dsn-scrollbar">
        <header>
            <div class="headefr-fexid" data-dsn-header="project">
                <div class="bg w-100" id="dsn-hero-parallax-img" data-dsn-ajax="img">
                    <div class="bg-image  cover-bg"
                        data-image-src="<?php echo base_url ('assets_frontend/img/home/header1.jpg')?>"></div>
                </div>

                <div class="scroll" data-dsn-animate="ajax">
                    <span class="background"></span>
                    <span class="triangle"></span>
                </div>
                <div class="project-title" id="dsn-hero-parallax-title">

                    <div class="title-text-header">
                        <span class="title-text-header-inner">
                            <span data-dsn-animate="ajax">Our Office</span>
                        </span>
                    </div>

                    <div class="sub-text-header" data-dsn-animate="ajax">
                        <h5>Kantor Pusat</h5>
                        <span>- Menara 165, Lantai 4</span>
                    </div>
                </div>

                <div class="project-page__inner">
                    <div class="h-100">
                        <div class="row justify-content-center align-items-end h-100">
                            <div id="descover-holder" class="col-lg-12 project-meta__content">
                                <div class="link">
                                    <a target="_blank"
                                        href="<?php echo site_url ('')?>"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="wrapper">
            <div class="root-project">
                <div class="container-fluid">
                    <div class="img-box-small dsn-parallax-full" data-dsn-grid="move-up" data-dsn-triggerhook="0">
                        <img src="<?php echo base_url ('assets_frontend/img/home/header2.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1.01">
                        <div class="cap">
                            <span>Meeting Room</span>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="img-box-small dsn-parallax-full" data-dsn-grid="move-up" data-dsn-triggerhook="0">
                        <img src="<?php echo base_url ('assets_frontend/img/home/header3.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1.01">
                        <div class="cap">
                            <span>Auditorium</span>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="img-box-small dsn-parallax-full" data-dsn-grid="move-up" data-dsn-triggerhook="0">
                        <img src="<?php echo base_url ('assets_frontend/img/home/header4.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1.01">
                        <div class="cap">
                            <span>Office Lounge</span>
                        </div>
                    </div>
                </div>

            </div>


        <?php
        include __DIR__ .('/../footer.php');
        include __DIR__ .('/../waitloader.php');
        include __DIR__ .('/../cursor.php');
        include __DIR__ .('/../js.php');
        include __DIR__ .('/../closebody.php');
        include __DIR__ .('/../end.php');
        ?>
