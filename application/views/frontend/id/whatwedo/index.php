<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root">
  <div id='my-scrollbar' data-scrollbar>

    <div class="box-seat">
      <div class="dsn-v-text">
        <div class="container-fluid">
          <div class="box-middle-text">
            <h2 data-dsn-animate="text">APA YANG KAMI LAKUKAN </h2>
            <p data-dsn-animate="text">Kami membentuk dan mendukung perilaku positif manusia melalui desain arsitektur, interior, lanskap, dan desain perkotaan. Kami mengkhususkan diri dalam desain hunian yang sustainable hingga desain gedung bertingkat tinggi untuk seluruh Asia. Melalui teknologi, kami akan membawa Anda untuk menemukan definisi Anda sendiri tentang apa artinya sustainable.</p>
          </div>
          <div class="inner-img" data-dsn-grid="move-up" data-overlay="9">
            <img class="has-top-bottom" src="<?php echo base_url ('assets_frontend/img/home/what_we_do.jpg')?>" alt="">
          </div>
        </div>
      </div>
    </div>

    <div class="wrapper">
      <div class="root-project">

        <div class="container intro-project section-margin">
          <div class="intro-text text-center">
            <div class="title-cover" data-dsn-grid="move-section" data-dsn-opacity="0.1"
            data-dsn-duration="170%" data-dsn-move="0%">
            APA YANG KAMI<br>LAKUKAN
          </div>
          <div class="inner">
            <h2 data-dsn-animate="text">Produk Kami</h2>
            <p data-dsn-animate="up">
              Kami menciptakan panel dinding 3D kami secara lokal dan bertanggung jawab untuk meminimalkan jejak karbon. Desain kami selaras dengan alam dan diresapi dengan filosofi.
            </p>
            <li>
              <ul>• Organik • Bertanggung jawab • Terjangkau</ul>
            </li>
            <li>
              <ul>• Dampak psikologis yang positif • Tahan lama • Kemudahan instalasi</ul>
            </li>
          </div>
        </div>
      </div>

      <div class="container-fluid gallery-col">
        <div class="row">
          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/1.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/1.jpg')?>" alt="">
              </a>
            </div>
          </div>

          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/2.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/2.jpg')?>" alt="">
              </a>
            </div>
          </div>

          <div class="col-md-4 box-im section-padding">
            <div class="image-zoom" data-dsn="parallax">
              <a class="single-image" href="<?php echo base_url ('assets_frontend/img/home/product/3.jpg')?>">
                <img src="<?php echo base_url ('assets_frontend/img/home/product/3.jpg')?>" alt="">
              </a>
            </div>
          </div>

        </div>
      </div>

      <div class="box-seat section-margin">
        <div class="container-fluid">
          <div class="inner-img" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/architecture_design_build.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
          <div class="pro-text" data-dsn-grid="move-section">
            <h4 data-dsn-animate="text"><b>Pembangunan Desain Arsitektur</b></h4>
            <p data-dsn-animate="text">
              Dengan mempercayakan kami sebagai kontraktor Anda, hal ini memungkinkan pengerjaan awal yang lebih cepat di lokasi, serta kontrol kualitas yang lebih baik untuk mewujudkan desain impian Anda.
            </p>
            <p data-dsn-animate="text">
              Sesuatu yang tak terduga sangat biasa terjadi pada pengerjaan proyek bangunan, oleh karena itu keterlibatan kami dapat memungkinkan proses desain yang dinamis, dimana kami dapat merinci ulang desain yang ada saat melakukan pembangunan.
            </p>
          </div>
        </div>
      </div>

      <div class="box-seat-v1 section-margin">
        <div class="container-fluid">
          <div class="pro-text" style="z-index:10" data-dsn-grid="move-section">
            <h4 data-dsn-animate="text"><b>Pembangunan Desain Interior</b></h4>
            <p data-dsn-animate="text">
    Kami memiliki tim internal yang terpercaya dan akan memberikan produk akhir yang berkualitas dengan garansi satu tahun.
            </p>
            <p data-dsn-animate="text">
    Kami menawarkan harga yang kompetitif untuk memastikan klien kami mendapatkan “hasil maksimal” dengan “lebih sedikit” pengeluaran.
            </p>
          </div>
          <div class="inner-img-v1" data-dsn-grid="move-up">
            <img src="<?php echo base_url ('assets_frontend/img/home/interior_design_build.jpg')?>" alt="" data-dsn-y="30%" data-dsn-scale="1">
          </div>
        </div>
      </div>

    </div>
    </div>



    <?php
    include __DIR__ .('/../footer.php');
    include __DIR__ .('/../waitloader.php');
    include __DIR__ .('/../cursor.php');
    include __DIR__ .('/../js.php');
    include __DIR__ .('/../closebody.php');
    include __DIR__ .('/../end.php');
    ?>
