<?php
include __DIR__ .('/../html.php');
include __DIR__ .('/../openbody.php');
include __DIR__ .('/../preloader.php');
include __DIR__ .('/../nav.php');
?>

<main class="main-root section-top">
  <div id="dsn-scrollbar">
    <header>
      <div class="container header-hero">
        <div class="row">
          <div class="col-lg-6">
            <div class="contenet-hero">
              <h5>HASIL KAMI</h5>
              <h2>Desain Portfolio Kreatif</h2>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="wrapper">
      <div class="root-work">
        <div class="container">
          <div class="box-title" data-dsn-title="cover">
            <h2 class="title-cover" data-dsn-grid="move-section" data-dsn-move="-70">Project</h2>
          </div>


          <div class="projects-list gallery">
            <?php foreach ($portfolio as $data): ?>
              <div class="item brand">
                <a href="#" class="effect-ajax" data-dsn-ajax="work"
                data-dsn-grid="move-up">
                <img class="has-top-bottom" src="<?=base_url()?>upload/portfolio/<?=$data->pic_file?>" alt="" />
                <div class="item-border"></div>
                <div class="item-info">
                  <h5 class="cat"></h5>
                  <h4><?php echo $data->pic_title?></h4>
                  <span><span><?php echo $data->pic_desc1?></span></span>
                </div>
              </a>
            </div>
          <?php endforeach ?>
      </div>
    </div>
  </div>


  <?php
  include __DIR__ .('/../footer.php');
  include __DIR__ .('/../waitloader.php');
  include __DIR__ .('/../cursor.php');
  include __DIR__ .('/../js.php');
  include __DIR__ .('/../closebody.php');
  include __DIR__ .('/../end.php');
  ?>
